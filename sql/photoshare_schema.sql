DROP SCHEMA photoshare;
CREATE SCHEMA photoshare;
USE photoshare;
CREATE TABLE photoshare.image(
id INT,
filename VARCHAR(255),
title VARCHAR(255),
category_id INT
);

CREATE TABLE photoshare.categorie(
id INT,
name VARCHAR(255)
);