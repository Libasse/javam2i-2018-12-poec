select customer.*, address.*
from customer inner join address
on customer.address_id = address.address_id;

select customer.last_name, address.address
from customer inner join address
on customer.address_id = address.address_id;

# titre du film , langage
select film.title, language.name
from film inner join language
on film.language_id = language.language_id;

# prenom, nom de l'acteur, l'id du film dans lequel il a joué
select actor.first_name, actor.last_name, film_actor.film_id
from actor inner join film_actor
on actor.actor_id = film_actor.actor_id;

# firstname (actor), last_name(actor), film_id dans lequel l'acteur d'id 1 a joué
select actor.first_name, actor.last_name, film_actor.film_id
from actor inner join film_actor
on actor.actor_id = film_actor.actor_id
where actor.actor_id = 1;

select actor.first_name, actor.last_name, film.title
from actor inner join film_actor
				on actor.actor_id = film_actor.actor_id
					and actor.actor_id = 1
			inner join film
				on film_actor.film_id = film.film_id;
 
# nom et prenom du client et son pays ( a refaire)

#select first_name, last_name, country
	#from customer inner join country
		#on customer.customer_id = country.country_id
        
# une table qui liste l'id, prenom, nom du custumer et les titres des films que le custumer a loué
# custumer_id, customer.first_name, customer.last_name, film.title
# film, customer, rental

select customer.customer_id, customer.first_name, customer.last_name, film.title
from  customer inner join rental
						on customer.customer_id = rental.customer_id
				inner join inventory
					on  rental.inventory_id = inventory.inventory_id
                    inner join film
                    on inventory.film_id = film.film_id;
                    
# quels sont les clients qui ont loue ACADEMY DINOSAUR
        
select customer.customer_id, customer.first_name, customer.last_name, film.title
from  customer inner join rental
						on customer.customer_id = rental.customer_id
				inner join inventory
					on  rental.inventory_id = inventory.inventory_id
                    inner join film
                    on inventory.film_id = film.film_id
                    and film.title = 'ACADEMY DINOSAUR';
# acteur n'ayant joué dans aucun film
select actor.actor_id, film_actor.film_id
from actor
	left outer join film_actor
    on actor.actor_id = film_actor.actor_id
    where film_actor.film_id IS NULL;
    
# acteur n'ayant pas joué dans le film d'id 1

select actor.*, film_actor.*
from actor
	left outer join film_actor
    on actor.actor_id = film_actor.actor_id
    and film_actor.film_id = 1
	where film_actor.actor_id IS NULL;


# jointue d'une table avec elle mêle = auto-jointure
# pas de condition de jointure => CROSS JOIN
select A1.*, A2.*
	from actor as A1
		inner join actor as A2;

select A1.*, A2.*
	from actor as A1
		cross join actor as A2;
        

