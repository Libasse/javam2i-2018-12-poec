#1)	Donner les numéro, nom et prénom de chaque client ainsi que les numéro, nom et prénom de leur représentant
# tables (numero_client.clients, nom.clients, prenom.clients, num_representant.clients) ( nom.representants, prenom.representants, num_representant.representants

select clients.numero_client, clients.nom, clients.prenom, clients.num_representant, representants.nom,  representants.prenom
from clients inner join representants
	on clients.num_representant = representants.num_representant;



#2)	Idem que 1) mais pour les clients dont la limite de crédit est de 1000$
select clients.numero_client, clients.nom, clients.prenom, clients.num_representant, representants.nom,  representants.prenom, clients.limite_credit
from clients inner join representants
	on clients.num_representant = representants.num_representant
		and clients.limite_credit = 1000;
# where clients.limite_credit = 1000


#3)	Pour chaque article de la commande, donner les numéros de commande et d’article, ainsi que la description, la quantité commandée, le prix facturé et le prix unitaire.

select lignes_commandes.numero_commande, lignes_commandes.numero_article, articles.description, lignes_commandes.quantite_commande, lignes_commandes.prix_facture, articles.prix_unitaire
	from lignes_commandes inner join commandes
		on lignes_commandes.numero_commande = commandes.numero_commande 
		inner join articles
		on lignes_commandes.numero_article = articles.numero_article;



#4)	Donnez le numéro de client, le numéro et la date de commande ainsi que le montant total de la commande pour chaque commande dont le montant est supérieur à 100$

select clients.numero_client, commandes.numero_commande, commandes.date_commande, (lignes_commandes.quantite_commande * lignes_commandes.prix_facture) as montant_total
 from clients inner join commandes
  on clients.numero_client = commandes.numero_client
		inner join lignes_commandes
         on commandes.numero_commande=lignes_commandes.numero_commande
          and (lignes_commandes.quantite_commande * lignes_commandes.prix_facture) > 1000;



#5)	Trouver chaque paire de client ayant un nom et un prénom identiques.
select C1.nom, C1.prenom
 from clients as C1
		inner join clients as C2
        on C1.numero_client = C2.numero_client;

# corrigé
select C1.nom, C1.prenom, C2.nom, C2.prenom
 from clients as C1
		inner join clients as C2
        #on C1.numero_client = C2.numero_client;
        on C1.nom = C2.nom and C2.prenom = C2.prenom;

select C1.numero_client, C1.nom, C1.prenom, C2.numero_client, C2.nom, C2.prenom
 from clients as C1
		inner join clients as C2
        #on C1.numero_client = C2.numero_client;
        on C1.nom = C2.nom and C2.prenom = C2.prenom and C1.numero_client > C2.numero_client;

#6)	Donner les noms et prénoms des clients ainsi que les articles qu’ils ont respectivement commandé. Si un client n’a pas commandé d’article il doit quand même apparaître comme client n’ayant pas commandé d’article.



#7)	Donner pour chaque article d’une commande : le numéro d’article, la quantité commandée, le numéro et la date de commande, les numéro, nom, prénom du client qui a passé la commande ainsi que les nom et prénom du représentant de ce client.
