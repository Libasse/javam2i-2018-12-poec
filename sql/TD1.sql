# 1)
SELECT numero_client, nom, prenom, solde
FROM clients
ORDER BY solde DESC;

# 2)

SELECT numero_client, nom, prenom, limite_credit
FROM clients
ORDER BY limite_credit DESC, nom DESC; # client ayant la même limite classés par ordre croissant

# 3)	compter le nombre d’articles de la catégorie EM.
SELECT count(*)
FROM articles
WHERE categorie = 'EM';

# 4)	trouver le nombre de clients total et leur solde total
 SELECT count(*), SUM(solde)
 FROM clients;

# 5)	donner les numéros de clients qui ont une commande ouverte en ne listant chaque client qu’une fois.
SELECT distinct numero_client
FROM commandes;

# 6)	donner la plus grande limite de crédit accordée à un client du représentant 06.

# 7)	donner une colonne correspondant à la concaténation du nom et du prénom pour tous les clients donc le nom de famille n’est pas Adams.
#8)	donner le montant des commandes de plus de 200$.

#9)	donner les valeurs de la limite de crédit et le nombre de clients pour chaque valeur, en ne listant que les limites de crédit détenues par plus d’un seul client.

# 10)	donner chaque limite de crédit et le nombre total de clients du représentant n°3 ayant cette limite.
# 
#11)	Donner la moyenne par ville des limites de crédit, de solde pour les clients des représentants 06 et 03. Les villes doivent être affichées en majuscule.
#Nommer les colonnes Ville, Moyenne_Limite_Credit et Moyenne_Solde.
