USE sakila;
SELECT *
FROM actor;
# *= toutes les colonnes
SELECT first_name
FROM actor;
-- commentair (ctrl+ /)

SELECT actor_id,first_name
FROM actor;

SELECT *
FROM actor
WHERE first_name = "PENELOPE" AND actor_id > 100;

# selectionner le titre de tous les films dont la duree depasse 1h
# selectionner l'id et le titre de tous les films dont la categorie est PG-13 et dont la duree depasse 1h
SELECT title
FROM film
WHERE length > 60;

SELECT film_id, title
FROM film
WHERE rating ='PG-13' AND length > 60;

# selectionner tous les clients qui ne sont pas du magasin 2
SELECT *
FROM customer
WHERE store_id != 2;
# selectionner tous les films dont la duree est comprise entre 50 minutes et 1h30
SELECT *
FROM film
WHERE length >= 50 AND length < 90;

# selectionner tous les films PG-13 et NC-17
SELECT *
FROM film
WHERE rating IN ('PG-13', 'NC-17');

SELECT *
FROM film
WHERE rating='PG-13' or rating= 'NC-17';

# tous les film qui durent entre 1h et 1h30
SELECT *
FROM film
WHERE length BETWEEN 60 AND 90;

# tous les clients dont le prenom commence par PE
SELECT *
FROM customer
WHERE first_name like "PE%";
# tous les clients dont le prenom termine par A
SELECT *
FROM customer
WHERE first_name like "%A";

# tous les clients dont le prenom contient As
SELECT *
FROM customer
WHERE first_name like "%AS%";

# tous les clients dont le prenom a pour deuxieme lettre A
SELECT *
FROM customer
WHERE first_name like "_A%";

# tous les clients dont le prenom a pour avant derniere lettre A
SELECT *
FROM customer
WHERE first_name like "%A_";

# TD
#1) donner les numeros, nom, prenom acteur
#2) lister la table pays complete
#3) donner le nom du pays 5
#4)donner le numero de chaque client dont le prenom est thomas
#5)donner le nom complet de chaque client

# 1)
SELECT actor_id, first_name, last_name
FROM actor;

#2)
SELECT *
FROM country;

#3)
SELECT country
FROM country
WHERE  country_id = 5;

#4)
SELECT customer_id
FROM customer
WHERE first_name = 'Thomas';

#5)
SELECT CONCAT(first_name, ' ', last_name)
FROM customer;

# titre de chaque film et la duree du film exprimee en heures

SELECT title, length/60  AS duration_in_hour
FROM film;

# afficher le prenom et la longueur  du prenom pour chaque acteur
# CHAR_LENGTH

SELECT first_name, CHAR_LENGTH(first_name)
FROM actor;

# Afficher les acteurs(*) dont le prenom contient >= 6 caracteres
SELECT *
FROM actor
WHERE char_length(first_name) >= 6;

SELECT title, 
	CASE  WHEN length > 90 THEN 'long'
    WHEN length BETWEEN 60 AND 90 THEN 'moyen'
		#WHEN length >= 60 AND length <=90 THEN 'moyen'
		ELSE 'court'
    END AS film_type
FROM film;

# fonction d'agregation

SELECT AVG(length), SUM(length), MIN(length), MAX(length)
FROM film
WHERE rating = 'PG-13';

SELECT first_name, (SELECT AVG(CHAR_LENGTH(first_name)))
FROM actor;

SELECT COUNT(*)
FROM ACTOR;

SELECT COUNT(length) # excepté valeurs nulles
FROM film;

SELECT COUNT(DISTINCT length)
FROM film;

SELECT DISTINCT first_name
FROM actor;

SELECT DISTINCT length
FROM film;


# GROUP BY
SELECT rating, AVG(length)
FROM film
GROUP BY rating;

# nombre de clients par magasin ( store_id, number_of_client)
#SELECT store_id, COUNT(*), JSON_ARRAYAGG(email)
SELECT store_id, COUNT(*), GROUP_CONCAT(email)
FROM customer
GROUP BY store_id;

# HAVING
SELECT rating,count(*)
FROM film
GROUP BY rating
HAVING COUNT(*)>200;

SELECT rating,count(*)
FROM film
WHERE length <150
GROUP BY rating
HAVING COUNT(*)>160;

# pour tous les prenoms, combien d'acteurs ont le meme prenom

SELECT first_name, count(*)
FROM actor
GROUP BY first_name;

# ORDER BY

SELECT *
FROM actor
#ORDER BY first_name ASC; pas besoin de mettre ASC , il l'est par defaut
#ORDER BY first_name DESC;
ORDER BY first_name DESC, last_name ASC;


SELECT first_name, count(*)
FROM actor
GROUP BY first_name
ORDER BY count(*) DESC;

SELECT first_name, count(*) as total
FROM actor
GROUP BY first_name
ORDER BY total DESC;




