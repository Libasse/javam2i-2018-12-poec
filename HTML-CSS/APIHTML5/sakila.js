
let state = {
    from:0,
    count:10
};

const xhr = new XMLHttpRequest();
xhr.open('GET', `http://localhost:8080/servlet-0.0.1-SNAPSHOT/api/films?from=${state.from}&count=${state.count}`);
xhr.onreadystatechange = function(e) {   
    if (xhr.readyState == XMLHttpRequest.DONE) {
        const data = JSON.parse(xhr.responseText);
        console.log(data);
        document.querySelector('body').innerHTML=filmsToHTML(data);
    }
}
xhr.send(null);

function filmsToHTML(films) {
    let html = '';
    html += '<table>';
    html +=' <thead><tr><th>Index</th><th>Titre</th></tr></thead>';
    html += '<tbody>';
    for (const f of films) {
        html += `<tr>
        <td>${f.film_id}</td>      
        <td><a href="api/film/${f.film_id}">${f.film_title}</a></td>   
        </tr>`;
    }
    html += '</tbody>';
    html +='</table>';
    html += '<a href="">next</a>';

    return html;
}

        

       
