console.log('avant');

// client 1: API XMLHttpRequest

const xhr = new XMLHttpRequest();
xhr.open('GET', 'http://localhost:8080/servlet-0.0.1-SNAPSHOT/api/hello');
xhr.onreadystatechange = function(e) {
    //console.log('dans onreadystatechange',xhr.readyState);
    if (xhr.readyState == XMLHttpRequest.DONE) {
        console.log(xhr.responseText);

        const data = JSON.parse(xhr.responseText);
        console.log(data);
        console.log(data["served-at"]);

        document.querySelector('body')
        .innerHTML=`<p>${xhr.responseText}</p>
        <p>served at: ${data["served-at"]}</p>`;
    }
}
xhr.send(null);
console.log('après');




// client 2: fetch
// client 3: jQuery