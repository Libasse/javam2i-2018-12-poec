const videoplayer = document.querySelector("video#player");

const buttonPlay = document.querySelector("button#play");

buttonPlay.addEventListener('click', e => {
    e.preventDefault();
    videoplayer.play();

});