package formation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "cible", eager = true)
@SessionScoped
public class Cible {
	
	int nbcoups = 0;
	int position_etoile;
	int selection = -1;
	int maxcases = 20;
	Random rnd = new Random();
	List<Integer> lescases = null ;
	List<Integer> selectionnees = null ;
	
	public Cible ()
	{
		position_etoile = rnd.nextInt(maxcases);
		lescases = new ArrayList<Integer>();
		selectionnees = new ArrayList<Integer>();
		for(int i =0;i <maxcases; i++)
		{
			lescases.add(i);
		}
	}

	public int getNbcoups() {
		return nbcoups;
	}

	public List<Integer> getLescases() {
		return lescases;
	}

	public void setSelection(int selection) {
		this.selection = selection;
	}
	
	
	 boolean gagne=false;
	public void selectionner(int unecase) {
		/*if (unecase ==position_etoile) gagne=true;
		else gagne=false;*/
		selectionnees.add(unecase);	
	}
	
	public String gagnante(int unecase) {
		//if (unecase ==position_etoile) return "red";
		 if (selectionnees.contains(unecase)) {
			 if (unecase ==position_etoile) return "red";
		 else return "yellow";
		 }
		else return "grey";
		
	}
	
	public void evaluer()
	{
		// reponse avec les couleurs
	}
	
}