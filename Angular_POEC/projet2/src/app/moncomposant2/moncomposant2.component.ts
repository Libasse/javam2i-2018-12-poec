import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-moncomposant2',
  templateUrl: './moncomposant2.component.html',
  styleUrls: ['./moncomposant2.component.css']
})
export class Moncomposant2Component implements OnInit {
	titre : any;
	cases : any;
	parite : any
  constructor ()
		{
            this.titre="AAA"
			this.cases=[]
            this.parite=""
            
            for (let l=0; l<20; l++)
			{
			let x=Math.round((Math.random()*100)+1)
                this.cases.push(x)
			}
        }
		
		ngOnInit() {
		}
		

        pairs()  {
           // console.log("pairs")
           this.parite="pairs"
        }

        impairs(){
            //console.log("impairs")
            this.parite="pairs"
        }


        visible (unecase)
        {
            if (this.parite === "") return "visible"
            if (this.parite === 'pairs')
            {
                if (unecase % 2 == 0) return 'visible'
                else return 'hidden'
            }
            else if (this.parite === 'impairs')
            {
                if (unecase % 2 != 0) return 'visible'
                else return 'hidden'
            }
           
        }

		
 

}
