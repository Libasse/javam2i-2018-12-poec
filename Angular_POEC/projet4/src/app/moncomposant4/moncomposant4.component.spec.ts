import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Moncomposant4Component } from './moncomposant4.component';

describe('Moncomposant4Component', () => {
  let component: Moncomposant4Component;
  let fixture: ComponentFixture<Moncomposant4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Moncomposant4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Moncomposant4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
