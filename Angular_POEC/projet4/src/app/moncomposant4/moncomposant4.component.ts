import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-moncomposant4',
  templateUrl: './moncomposant4.component.html',
  styleUrls: ['./moncomposant4.component.css']
})
export class Moncomposant4Component implements OnInit {
  
	titre : any;
	grille : any;
	selectionnees : any;
	casesgagnantes: any;
	constructor ()
			
		{
			this.titre = 'Jeu du Loto'
			this.grille = []
            this.selectionnees=[]
            this.casesgagnantes=[]

			for (let l=0; l<10; l++)
			{
				let ligne =  []
				for (var c=0; c<5; c++)
				{
					ligne.push(l + c * 10)
				}
				this.grille.push(ligne)
			}
			//console.log(this.grille)
		}

        visible(unecase){
            return unecase==0 ? "hidden":"visible"
        }

        selectionner (unecase)
        {
            if(this.selectionnees.indexOf(unecase) ==-1)
            {
                this.selectionnees.push(unecase)
            }
           
            console.log( this.selectionnees)
        }

        couleur(unecase){
            let c = 'white'
            if(this.selectionnees.indexOf(unecase) !=-1){
                c='yellow'
            }
            return c
        }
      
        letirage(){
            this.tirage=[]
            for (var i=0;i<6;i++){
                var v= Math.round(Math.random()*49)+1
                this.tirage.push(v)
            }
        }      
       		
		legain()
		{
			this.casesgagnantes = []
			for  (let unecase in this.selectionnees)
			{
				if (this.tirage.indexOf(this.selectionnees[unecase])!=-1)
				{
					this.casesgagnantes.push(this.selectionnees[unecase])
				}
			}
			console.log(this.casesgagnantes)
		}

        gagnante(unecase){
            let border="1px 1px 1px 1px"
            if(this.casesgagnantes.indexOf(unecase) !=-1){
                border="4px 4px 4px 4px"

            }
            return border
        
		}
	  ngOnInit() {
	  }

}		
