import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Moncomposant5Component } from './moncomposant5.component';

describe('Moncomposant5Component', () => {
  let component: Moncomposant5Component;
  let fixture: ComponentFixture<Moncomposant5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Moncomposant5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Moncomposant5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
