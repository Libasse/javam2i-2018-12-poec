import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-moncomposant5',
  templateUrl: './moncomposant5.component.html',
  styleUrls: ['./moncomposant5.component.css']
})


export class Moncomposant5Component implements OnInit {
	
			
			titre :any;
			cases : any;
			timer :any;
			lignecase : any;
			colonnecase : any;
			

		constructor ()
		{
			
			this.titre = 'Grille'
			this.cases = []
			this.timer ='';
			this.lignecase = 0;
			this.colonnecase = 0;
			
			for (let l=0; l<10; l++)
			{
				let ligne =  []
				for (var c=0; c<5; c++)
				{
					ligne.push(l + c * 10)
				}
				this.cases.push(ligne)
			}
		}
		
		start(v)
		{
			if(this.timer!='') clearInterval (this.timer)
			if (v === "HB")
			{
				this.timer=setInterval(
						() =>{ 
							//console.log(that)
							this.defilerHB (); 						
						}
				, 90);
			}
			else if (v === "BH")
			{
				this.timer=setInterval(
						() =>{ 
							//console.log(that)
							this.defilerBH (); 						
						}
				, 90);
			}
		}
		
		defilerHB ()
		{
			this.colonnecase++;
			if(this.colonnecase>=5) { 
				this.colonnecase = 0
				this.lignecase++
				if(this.lignecase >=10 ) { 
					this.lignecase=0
				}
			}
			this.$apply()
		}
		
		defilerBH ()
		{
		}

		couleur(unecase)
		{
			if( unecase === this.cases[this.lignecase][this.colonnecase]) return "yellow"
			else return "white"
		}
		
		ngOnInit() {
		}
		
		
	}

  


