import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Moncomposant5Component } from './moncomposant5/moncomposant5.component';
import { Moncomposant6Component } from './moncomposant6/moncomposant6.component';

@NgModule({
  declarations: [
    AppComponent,
    Moncomposant5Component,
    Moncomposant6Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
