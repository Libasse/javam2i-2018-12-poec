import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Moncomposant6Component } from './moncomposant6.component';

describe('Moncomposant6Component', () => {
  let component: Moncomposant6Component;
  let fixture: ComponentFixture<Moncomposant6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Moncomposant6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Moncomposant6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
