import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-moncomposant6',
  templateUrl: './moncomposant6.component.html',
  styleUrls: ['./moncomposant6.component.css']
})
export class Moncomposant6Component implements OnInit {

  titre: any;
  cases: any;
  timer: any;
  lignecase: any;
  colonnecase: any;


  constructor() {

    this.titre = 'Pacman';
    this.cases = [];
    this.timer = '';
    this.lignecase = 0;
    this.colonnecase = 0;

    for (let l = 0; l < 10; l++) {
      let ligne = [];
      for (let c = 0; c < 5; c++) {
        ligne.push('');
      }
      this.cases.push(ligne);
    }
  }

  start(v) {
    if (this.timer != '') clearInterval(this.timer)
    if (v === 'HB') {
      this.timer = setInterval(
        () => {
          this.defilerHB();
        }
        , 90);
    }
  }

  defilerHB() {
    this.colonnecase++;
    if (this.colonnecase >= 5) {
      this.colonnecase = 0;
      this.lignecase++;
      if (this.lignecase >= 10) {
        this.lignecase = 0;
        this.colonnecase = 0;
      }
    }
  }

  image(unecase) {
    if (unecase === this.cases[this.lignecase][this.colonnecase]) {
      return '';
    }
    else {
      return '';
    }


  }

  ngOnInit() {
  }


}
