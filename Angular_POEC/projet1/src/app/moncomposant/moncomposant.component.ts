import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-moncomposant',
  templateUrl: './moncomposant.component.html',
  styleUrls: ['./moncomposant.component.css']
})
export class MoncomposantComponent implements OnInit {

	constructor ()
	{
		this.titre = 'Jeu du Loto'
		this.cases = []
		this.couleur = 'white'
		for (let l=0; l<20; l++)
		{
			this.cases.push(l)
		}
	}
	
	ngOnInit()
	{
	}
	
	changeCouleur (couleur)
	{
		this.couleur = couleur
	}

}

  


