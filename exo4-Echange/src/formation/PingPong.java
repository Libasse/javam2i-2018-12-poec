package formation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "pingpong", eager = true)
@SessionScoped
public class PingPong{
	String egauche="AA";
	public String getEgauche() {
		return egauche;
	}
	public String getEdroite() {
		return edroite;
	}
	
	String edroite="BB";
	public void deplaceGD()
	{
		edroite = egauche;
		egauche = "";		
	}
	public void deplaceDG()
	{
		egauche = edroite;
		edroite = "";
	}
}