package formation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {

	
		
		 public static void main(String[] args) throws Exception {
		     ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		      // Acces au bean
		      DataFile df = (DataFile ) context.getBean("df");
		      // Methode du bean
		     //DataFile df = new DataFile("dataFile.txt");
		      System.out.println (df.getContenu());
		      
		   }

}