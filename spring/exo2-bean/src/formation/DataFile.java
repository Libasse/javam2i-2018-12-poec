package formation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;



public class DataFile {

	private String fichier;
	private String contenu ="";
	 
	   public DataFile (String fichier)
	   {
	      this.fichier = fichier;
	   }
	   
	   
	 
	   public String getContenu() throws Exception{
	    // ouvrir le fichier et retourner le contenu
		   BufferedReader f = new BufferedReader(new FileReader(fichier));
		   while (true) {
			   String ligne = f.readLine();
			   if (ligne == null) break;
			   contenu += ligne + "\n";
		   }
		   f.close();
		   return contenu;
	   }

}
