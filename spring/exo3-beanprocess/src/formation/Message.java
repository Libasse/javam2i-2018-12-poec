package formation;

public class Message {

	private String contenu;
	 
	   public Message ()
	   {
	      System.out.println("Construction du Bean");
	   }
	   
	   public void setContenu(String message){
	      this.contenu  = message;
	   }
	 
	   public String getContenu(){
	    return "Le message: " + contenu;
	   }

}
