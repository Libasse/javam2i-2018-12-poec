package formation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import javax.sql.DataSource;

public class TotoDAOserviceImpl implements TotoDAOservice {

	JdbcTemplate jt = null;
	
	@Autowired
	public void setJt(DataSource ds) {
		jt = new JdbcTemplate(ds);
	}
	
	@Autowired
	DataSource ds = null;
	
	public TotoDAOserviceImpl ()
	{
		
	}
	
	@Override
	public void remplir(int max) {
		// TODO Auto-generated method stub
		for (int i=0; i<max;i++) {
			ajouter (new Toto (0, i, "TOTO" + i, "toto" + i));
		}
		
	}

	@Override
	public void ajouter(Toto f) {
		jt = new JdbcTemplate(ds);
		String SQL = "insert into totos (id, age, nom, prenom) values (?, ?, ?, ?)";
		jt.update( SQL, new Object [] {f.getId(), f.getAge(), f.getNom(),f.getPrenom() } );		
	}

	@Override
	public List<Toto> findAll() {
		
		String SQL = "select * from totos";
		List<Toto> totos = jt.query(SQL, new TotoMapper());
		return totos; 
	}

}