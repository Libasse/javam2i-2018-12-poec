


package formation;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {
   public static void main(String[] args) {
	  // Autowire au niveau des setter/getter
      ApplicationContext context =  new ClassPathXmlApplicationContext("Beans.xml");
     TotoDAOservice fds = (TotoDAOservice)context.getBean("fds");
     //fds.remplir(1000);
     //fds.ajouter(new Toto());
     System.out.println(fds);
     for (Toto f: fds.findAll()) {
    	 System.out.println(f.toString());
     }
   }
}