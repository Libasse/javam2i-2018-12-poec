package formation;

import java.util.List;

public interface TotoDAOservice {
	void remplir(int max);
	void  ajouter(Toto f);
	List<Toto> findAll();

}
