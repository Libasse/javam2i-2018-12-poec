package formation;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

// ORM mettant en correspondance un resultaset de la base
// avec une instance d'objet formation
public class TotoMapper implements RowMapper<Toto> {
	   public Toto mapRow(ResultSet rs, int rowNum) throws SQLException {
		  Toto toto = new Toto();
		  toto.setId(rs.getInt("id"));
		  toto.setAge(rs.getInt("age"));
		  toto.setNom(rs.getString("nom"));
		  toto.setPrenom(rs.getString("nom"));
	      return toto;
	   }
}
