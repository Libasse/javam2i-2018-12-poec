package formation;
 
import org.springframework.beans.factory.annotation.Autowired;
 
public class Personne {
 
    private int id;
    private String nom;
    private String prenom;
    private int age;
    
    public String getNom() {
        return nom;
    }
    @Autowired
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getPrenom() {
        return prenom;
    }
    @Autowired
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public int getAge() {
        return age;
    }
    @Autowired
    public void setAge(int age) {
        this.age = age;
    }
 
    public int getId() {
        return id;
    }
    @Autowired
    public void setId(int id) {
        this.id = id;
    }  
    public Personne ()
    {
        /*
         nom="VIDE";
        prenom="vide";
        age = -1;*/
    }
    Personne (
            String nom,
            String prenom,
            int age)
    {
        this.nom=nom;
        this.prenom=prenom;
        this.age = age;
    }
    String getAll ()
    {
        return String.format("%s %s %d", nom, prenom, age);
    }
}