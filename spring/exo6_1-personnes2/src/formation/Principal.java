


package formation;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {
   public static void main(String[] args) {
	  // Autowire au niveau des setter/getter
      ApplicationContext context =  new ClassPathXmlApplicationContext("Beans.xml");
      Personne p1 = (Personne) context.getBean("p1");
      System.out.println(p1.getAll());
   }
}