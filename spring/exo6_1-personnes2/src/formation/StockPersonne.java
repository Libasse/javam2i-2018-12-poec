package formation;
import java.util.LinkedList;
import java.util.List;

public class StockPersonne {

	List<Personne> data = new LinkedList<Personne>();
	public void setItem (Personne p) {
		data.add(p);
	}
	public void setItems (Personne [] lp) {
		for (Personne p : lp) data.add(p);
	}
	public List<Personne> getAllItems ()
	{
		return data; 
	}
}