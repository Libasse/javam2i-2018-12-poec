package formation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/")
public class ControllerDispatcher {
	
	//@Autowired
	Deviner deviner;
	
	@Autowired
	ApplicationContext context;
   
	@Autowired
	public void setContext(ApplicationContext context) {
		this.context = context;
		deviner = (Deviner)context.getBean("deviner");
		System.out.println("set context");
		System.out.println(context);
	}
	
 
	public  ControllerDispatcher() {
		
	}
	
	/*@RequestMapping("/index")
	public String index() {
		System.out.println("index");
		return "index";
	}
	
	@RequestMapping("/valider")
	
	public ModelAndView valider(@RequestParam ("valeur") int valeur) {
		String data= deviner.getEvaluation(valeur);
		System.out.println("valider");
		System.out.println(deviner.getEvaluation(valeur));
		return new ModelAndView("index","data",data);
	}*/
	
	@RequestMapping("/")
	public ModelAndView valider (@RequestParam (value="valeur",defaultValue="-1") int valeur) {
		ModelAndView v =new  ModelAndView("index");
		String message ="";
		if (valeur==-1) {
			message="<<<Bienvenue au jeu de Devinette>>>";
			deviner.reset();
		} else {
			message=deviner.getEvaluation(valeur);
		}
		v.addObject("message",message);
		return v;
		
	}
}
