package formation;

import org.springframework.beans.factory.annotation.Autowired;

public class Deviner {
	int rebours =10;
	
	@Autowired
	DevinetteDAOservice devinetteDAOservice;
	
	public Deviner() {
		
	}

	public int getRebours() {
		return rebours;
	}
	
	public String getEvaluation(int v) {
		System.out.println(devinetteDAOservice.getValeur());
		String message="";
		rebours--;
		// comparaison avec le nombre secret;
		if (v>devinetteDAOservice.getValeur()) message="trop grand";
		else if (v<devinetteDAOservice.getValeur()) message="trop petit";
		else {
			message= "Bravo ! gagné en "+ (10-rebours) + "coup(s)";
		}
		rebours--;
		if (rebours==0) message = "Dommage ! devinetteDAOservice = "+ devinetteDAOservice.getValeur();
		return message;
		
		
	}

	public void reset() {
		// TODO Auto-generated method stub
		rebours=10;
		devinetteDAOservice=new DevinetteDAOservice();
		
	}
	
	

}
