package formation;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class ControlerBonjour {
	public  ControlerBonjour() {
		System.out.println("Controller OK");
	}
	
	@RequestMapping("/index")
	public String index() {
		return "index";
	}
	
	@RequestMapping("/index2")
	@ResponseBody
	public String index2() {
		return "<h1>Bonjour tout le monde</h1>";
	}

	
	@RequestMapping(value="/index3", method=RequestMethod.GET)
	@ResponseBody
	public String index3(@RequestParam String aqui) {
		return "<h1>Bonjour à toi" + aqui+ "</h1>";
	}
	
	@RequestMapping(value="/index4", method=RequestMethod.GET)
	@ResponseBody
	public String index3(@RequestParam String message, @RequestParam int max) {
		String buff="";
		for (int i = 0; i<max ;i++) {
		buff += String.format( "<h3>%s -- %d</h3>",message,i);
	}
		return buff;
	}

	
	@RequestMapping(value="/index5", method=RequestMethod.GET)	
	public ModelAndView index5(@RequestParam String message, @RequestParam int max) {
		List<String> data = new ArrayList<>();
		for (int i = 0; i<max ;i++) {
		data.add(String.format( "%s -- %d",message,i));
	}
		return new ModelAndView("reponse","data",data);
	}
	
}
