package formation;

import java.util.List;

public interface FormationDAOservice {
	
	void remplir(int max);
	void  ajouter(Formation f);
	List<Formation> findAll();
	Formation find(int id);

}
