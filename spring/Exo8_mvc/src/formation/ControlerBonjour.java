package formation;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class ControlerBonjour {
	
	ApplicationContext context;
    FormationDAOservice fds;
	
	public  ControlerBonjour() {
		System.out.println("Controller OK");
		context =  new ClassPathXmlApplicationContext("Beans.xml");
	    fds = (FormationDAOservice)context.getBean("fds");
	}
	
	@RequestMapping("/index")
	public String index() {
		return "index";
	}
	
	@RequestMapping("/lister")
	@ResponseBody
	public ModelAndView lister() {
		return new ModelAndView ("liste", "formations", fds.findAll());
	}

	@RequestMapping("/ajouter")
	@ResponseBody
	public String ajouter() {
		return "ajout";
	}
	
	
}
