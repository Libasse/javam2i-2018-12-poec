package formation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import org.springframework.beans.factory.annotation.Autowired;



public class DataFile {
	
	@Autowired
	private String fichier=null;
	private String contenu ="";
	 
	   public DataFile (String fichier)
	   {
	      this.fichier = fichier;
	      System.out.println("Une fois");
	   }
	   
	   public DataFile ()
	   {
	      this.fichier = "";
	      System.out.println("Une fois");
	   }
	   
	 
	   public String getContenu() throws Exception{
	    // ouvrir le fichier et retourner le contenu
		   BufferedReader f = new BufferedReader(new FileReader(fichier));
		   while (true) {
			   String ligne = f.readLine();
			   if (ligne == null) break;
			   contenu += ligne + "\n";
		   }
		   f.close();
		   return contenu;
	   }

}
