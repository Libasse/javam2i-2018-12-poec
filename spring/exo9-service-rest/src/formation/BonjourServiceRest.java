package formation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bonjour")
public class BonjourServiceRest {
	
	@RequestMapping(value="/{aqui}", method=RequestMethod.GET,produces="application/json")
	public String ditBonjour(@PathVariable String aqui) {
		return  "{ message : 'Bonjour � " + aqui + "'}";
	}
}
