package formation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {

	
		
		 public static void main(String[] args) throws Exception {
		     ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		      // Acces au bean
		      Message obj = (Message ) context.getBean("msg");
		      // Methode du bean
		      System.out.println (obj.getContenu());
		      obj = null;
		     ((ClassPathXmlApplicationContext) context).close();
		     Thread.sleep(3000);
		   }

}