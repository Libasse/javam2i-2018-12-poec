<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
    		prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
td {
	font-size : 33px;
	background-color : cyan; 
}
</style>
<script>
var c = <c:out value='${complete}'/>
console.log(c)
</script>
</head>
<body>
	<table border='<c:out value="${complete}"/>'>
		<c:forEach begin="0" end="${maxligne-1}" var="ligne">
			<tr>
			<c:forEach begin="0" end="${maxcolonne-1}" var="colonne">			
			 <c:set var="epaisseur" value="1px"/>
                <c:forEach items='${indices}' var='pair'>
                    <c:if test="${pair[0] == ligne && pair[1] == colonne}">
                        <c:set var="epaisseur" value="10px"/>
                    </c:if>
                </c:forEach>				
			
				<td style='background-color:${grille.etat[ligne][colonne]};border: ${epaisseur} solid;'>
				<c:if test="${ligne+colonne != 0}">
					<a href="?ligne=${ligne}&colonne=${colonne}"><c:out value='${grille.cases[ligne][colonne]}'/></a>
				</c:if>
				</td>
			</c:forEach>
			</tr>
		</c:forEach>
		<tr><td colspan="5"><a href="tirage">Tirage</a></td></tr>
		<tr><td colspan="5">
			<table border="1">
			<tr>
				<c:forEach items="${tirage}" var="lacase">
				<td>
					<c:out value='${lacase}'/>
				</td>
				</c:forEach>
			</tr>		
			</table>
		</td></tr>
		<tr><td colspan="5"><a href="gain">Gain</a></td></tr>
	</table>
	
</body>
</html>