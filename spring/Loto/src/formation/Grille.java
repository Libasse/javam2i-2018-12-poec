package formation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;

public class Grille {

	int cases [][] = new int [10][5];
	String etat [][] = new String [10][5];
	
	public String[][] getEtat() {
		return etat;
	}

	public int[][] getCases() {		
		return cases;
	}
	
	public Grille ()
	{	
		System.out.println("charger grille");
		System.out.println(cases.length);
		System.out.println(cases[0].length);
		for (int ligne=0; ligne<cases.length; ligne++)
		{
			for (int colonne=0; colonne<cases[0].length; colonne++)
			{
				cases [ligne][colonne] = ligne + colonne * 10;
				etat [ligne][colonne]="white";
			}
		}		
	}
	
public boolean getComplete() {		
		int compteur=0;
		for (int ligne=0; ligne<cases.length; ligne++) {
			
			for (int colonne=0; colonne<cases[0].length; colonne++) {
				if(etat [ligne][colonne]=="yellow") compteur++;
			}
		}
		return compteur >=6;
	}


public List<Integer[]> evaluer(List<Integer> tirage)
{
	List<Integer[]> lescases = new ArrayList<Integer[]>();
	for (int ligne=0; ligne<cases.length; ligne++)
	{
		for (int colonne=0; colonne<cases[0].length; colonne++)
		{
			if (etat [ligne][colonne] == "yellow" 
					&& tirage.contains(cases [ligne][colonne] )) 
				lescases.add(new Integer [] {ligne, colonne});
		}
	}
	return lescases;		
}

}