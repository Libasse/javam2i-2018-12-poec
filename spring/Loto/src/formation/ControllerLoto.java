package formation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class ControllerLoto {
	
	Grille grille;
	ArrayList<Integer> tirage;
	ApplicationContext context=null;
	
	@Autowired
	public void setContext(ApplicationContext context) {
		this.context = context;
		grille = (Grille)context.getBean("grille");
	}

	public  ControllerLoto() {
		
	}
	

	@RequestMapping("/tirage")
	public ModelAndView tirage () {
		// generer 6 valeurs aleatoires uniques
		 tirage= new ArrayList<>();
		Random rnd = new Random();
		while (tirage.size() !=6) {
			int v =rnd.nextInt(49)+1;
			if(!tirage.contains(v))tirage.add(v);
		}
		ModelAndView view =new  ModelAndView("index");
		view.addObject("complete", 1);
        if (grille.getComplete ())
        {
            view.addObject("complete", 5);
        }
		view.addObject("maxligne",grille.cases.length);
		view.addObject("maxcolonne",grille.cases[0].length);
		view.addObject("grille",grille);
		view.addObject("tirage",tirage);
		return view;		
	}
			
	@RequestMapping("/gain")
	public ModelAndView gain (@RequestParam (value="ligne",defaultValue="-1") int ligne,
			@RequestParam (value="colonne",defaultValue="-1") int colonne) {
		//System.out.println("gains");
		
		ModelAndView v =new  ModelAndView("index");	
		// unecase = {ligne, colonne}
		List<Integer[]> lescases = grille.evaluer(tirage);
		v.addObject("complete", 1);
		if (grille.getComplete ())
		{
			v.addObject("complete", 5);
		}
		else
		{
			// Mettre a jour la grille
			if (grille.etat[ligne][colonne] == "yellow")
			{
				grille.etat[ligne][colonne] = "white";
			}
			else 
			{
				grille.etat[ligne][colonne] = "yellow";
			}
		}
		v.addObject("maxligne", grille.cases.length);
		v.addObject("maxcolonne", grille.cases[0].length);
		v.addObject("grille", grille);
		v.addObject("tirage", new ArrayList<Integer>());
		v.addObject("indices", lescases);
		v.addObject("tirage", tirage);
		return v;
	}
	
	
	@RequestMapping("/")
	public ModelAndView valider (
			@RequestParam (value="ligne",defaultValue="-1") int ligne,
			@RequestParam (value="colonne",defaultValue="-1") int colonne) {
		ModelAndView v =new  ModelAndView("index");
		
		if (ligne+colonne !=-2) {
			v.addObject("complete",1);
			if(grille.getComplete()) {
				v.addObject("complete",5);
			}else {
			
			
			if (grille.etat[ligne][colonne]=="yellow") {
				grille.etat[ligne][colonne]="white";
			} else {
				grille.etat[ligne][colonne]="yellow";
			}
		}
		}
		v.addObject("maxligne",grille.cases.length);
		v.addObject("maxcolonne",grille.cases[0].length);
		v.addObject("grille",grille);
		return v;
		
	}
	
}

