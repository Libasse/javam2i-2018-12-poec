package formation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import javax.sql.DataSource;

public class FormationDAOserviceImpl implements FormationDAOservice {

	JdbcTemplate jt = null;
	
	@Autowired
	public void setJt(DataSource ds) {
		jt = new JdbcTemplate(ds);
	}
	
	@Autowired
	DataSource ds = null;
	
	public FormationDAOserviceImpl ()
	{
		
	}
	
	@Override
	public void remplir(int max) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ajouter(Formation f) {
		jt = new JdbcTemplate(ds);
		String SQL = "insert into formations (id, libelle) values (?, ?)";
		jt.update( SQL, new Object [] {f.getId(), f.getLibelle() } );		
	}

	@Override
	public List<Formation> findAll() {
		
		String SQL = "select * from formations";
		List<Formation> formations = jt.query(SQL, new FormationMapper());
		return formations; 
	}

}