


package formation;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {
   public static void main(String[] args) {
	  // Autowire au niveau des setter/getter
      ApplicationContext context =  new ClassPathXmlApplicationContext("Beans.xml");
     FormationDAOservice fds = (FormationDAOservice)context.getBean("fds");
     //fds.remplir(100);
     fds.ajouter(new Formation("FORMATION RAMBO"));
     System.out.println(fds);
     for (Formation f: fds.findAll()) {
    	 System.out.println(f.toString());
     }
   }
}