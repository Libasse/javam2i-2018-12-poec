package formation;
 
import org.springframework.beans.factory.annotation.Autowired;
 
public class Formation {
 
    private int id;
    private String libelle;
  
    public int getId() {
		return id;
	}

    public Formation () {
    	
    }

    public Formation (String libelle) {
    	this.libelle = libelle;
    }

	public void setId(int id) {
		this.id = id;
	}




	public String getLibelle() {
		return libelle;
	}




	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String toString ()
    {
        return String.format("%d %s", id, libelle);
    }
}