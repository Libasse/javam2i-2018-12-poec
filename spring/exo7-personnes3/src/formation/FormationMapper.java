package formation;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

// ORM mettant en correspondance un resultaset de la base
// avec une instance d'objet formation
public class FormationMapper implements RowMapper<Formation> {
	   public Formation mapRow(ResultSet rs, int rowNum) throws SQLException {
		  Formation formation = new Formation();
		  formation.setId(rs.getInt("id"));
		  formation.setLibelle(rs.getString("Libelle"));
	      return formation;
	   }
}
