


package formation;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {
   public static void main(String[] args) {
	  // Dans beans.xml, des beans sont cr��s
	  // moins de code � faire : C'est de l'injection d'objet
      ApplicationContext context =  new ClassPathXmlApplicationContext("Beans.xml");
      Personne p1 = (Personne) context.getBean("p1");
      System.out.println(p1.getAll());
      Personne p2 = (Personne) context.getBean("p2");
      System.out.println(p2.getAll());
      StockPersonne ps = (StockPersonne) context.getBean("ps");
      for (Personne p : ps.getAllItems()) System.out.println(p.getAll ());
   }
}