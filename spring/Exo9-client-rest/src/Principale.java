import java.io.IOException;
import java.util.HashMap;


import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import java.util.ArrayList;


/*public class Principale {
	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		HttpClient client = new DefaultHttpClient() ;		
		// Contacter le serveur
		HttpHost host = new HttpHost("10.130.10.18",8080,"http");
		HttpGet request = new HttpGet("/exo9-service-rest/bonjour/libasse");
		
		HttpResponse response = client.execute(host,request);
		
		// extraire la reponse
		HttpEntity entite=response.getEntity();
		System.out.println(EntityUtils.toString(entite));
	}
}*/

public class Principale {

    public static void main(String[] args) throws Exception {
        
        HttpClient client = new DefaultHttpClient ();
        String [][] annuaire = {
            {"Jacob","Exo12_client_rest","10.130.10.35"},
            {"Salem","exo11-serviceRest","10.130.10.48"},
            {"momos","/","10.130.10.51"},
            {"Nicolas","exo10-service-rest","10.130.10.17"},
            {"jj","exo10-service-rest","10.130.10.25"},
            {"Libasse","exo9-service-rest","10.130.10.18"},
            {"Guillaume","Exo10-RESTService","10.130.10.60"},
            {"Tristan","exo11_service_rest","10.130.10.33"},
            {"Jacob","Exo11_service_rest","10.130.10.35"},
            {"Rabie","Exercice11_service_Rest","10.130.10.42"},
            {"franck","service_REST","10.130.10.52"},
            {"David","exo91-service-rest","10.130.10.23"}
        };
        HttpHost host ;
        HttpGet request;
        HttpResponse response;
        
        // Contacter le server
        for (int i =0; i<annuaire.length; i++)
        {
            try {
            host = new HttpHost(annuaire[i][2] , 8080, "http");
            request = new HttpGet("/" + annuaire[i][1]+ "/bonjour/karim");
            response = client.execute(host, request);        
            // exrtraire la r�ponse
            HttpEntity entite = response.getEntity();
            System.out.println(annuaire[i][0] + "::" + EntityUtils.toString (entite));        
            }
            catch (Exception e) {
                System.out.println("Deconner : " + annuaire[i][2]);
            }
        }
    }

}
