package formation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "grille", eager = true)
@SessionScoped
public class Grille {
	
	List<List<Integer>> lignes = new ArrayList<List<Integer>> ();
	List<Integer> selectionnees = new ArrayList<Integer> ();
	List<Integer> gagnantes= new ArrayList<Integer> ();
	int maxlignes;
	int maxcolonnes;

	public Grille ()
	{
		this.maxlignes = 10;
		this.maxcolonnes = 5;
		for (int i=0; i<maxlignes; i++)
		{
			List<Integer> ligne = new ArrayList<> ();
			for (int j=0; j<maxcolonnes; j++)
			{
				ligne.add(i + j * 10);
			}
			lignes.add(ligne);
		}
	}
	public List<List<Integer>> getLignes() {
		return lignes;
	}
	public int getMaxlignes() {
		return maxlignes;
	}
	public int getMaxcolonnes() {
		return maxcolonnes;
	}

	public void selection(int unecase)
	{
		if (!selectionnees.contains(unecase) && selectionnees.size() != 6)
		{
			selectionnees.add(unecase);
		}
	}
	
	public String couleur(int unecase)
	{
		if (selectionnees.contains(unecase)) return  "yellow";
		else return "gray";
	}
	
	public String gagnante (int unecase)
	{
		if (gagnantes.contains(unecase)) return  "5 5 5 5";
		else return "0 0 0 0";
		
	}
	
	List<Integer> tirage = null;
	public List<Integer> getTirage() {
		return tirage;
	}
	Random rnd = new Random ();
	public void fairetirage()
	{
		tirage = new ArrayList<>();
		while(tirage.size() != 6)
		{
			int v = rnd.nextInt(49) + 1;
			if (!tirage.contains (v)) tirage.add(v);
		}
	}
	public void gain ()
	{
		gagnantes.clear();
		for(int v : selectionnees)
		{
			if (tirage.contains(v)) gagnantes.add(v);
		}
	}
}