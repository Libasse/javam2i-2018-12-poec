package com.m2i.training.javaee.photoshare;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.m2i.training.javaee.photoshare.Image;
import com.m2i.training.javaee.photoshare.Image;

public class ImageRepository {
	
	
	public List<Image> findAll() {
		List<Image> images = new ArrayList<Image>();

		try (Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/photoshare?serverTimezone=UTC",
				"root", "rootroot")) {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM image");
			while (rs.next()) {
				Image image = new Image(rs.getInt("id"), rs.getString("filename"), rs.getString("title"),
						rs.getInt("category_id"));
				images.add(image);
			}
		} catch (SQLException e) {
			// en pratique on va plutôt créer une RepositoryException
			throw new RuntimeException(e);
		}
		
		return images;
	}
	
	
	public Image find(int id) {
		try (Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/photoshare?serverTimezone=UTC",
				"root", "rootroot")) {
			
			// NON !!!
			// ResultSet rs = stmt.executeQuery(
			//		"SELECT * FROM film WHERE film_id = " + id);
			
			PreparedStatement stmt = 
					con.prepareStatement(
							"SELECT * FROM image WHERE id = ?");
			stmt.setInt(1, id);
			
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()) {
				Image image = new Image(rs.getInt("id"), 
									 rs.getString("filename"), 
									 rs.getString("title"),
									 rs.getInt("category_id"));
				return image;
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			// en pratique on va plutôt créer une RepositoryException
			throw new RuntimeException(e);
		}
	}
	
	
	

}
