package formation;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "fichier", eager = true)
@SessionScoped
public class Fichier {
	
   private String nom="data.txt";
   private String contenu="VIDE";
   
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}


public String getContenu() {
    contenu = "";
    String path = new File("/").getAbsolutePath();
    try {
        FileReader fr = new FileReader(new File(path+nom));
        while (true)
        {
            int r;
            r = fr.read();
            if (r=='\n')contenu+="<br/>";
            if(r == -1) break;
            contenu += (char)r;
        }
        fr.close();
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    return contenu;
}

	
	


public void setContenu(String contenu) {
	this.contenu = contenu;
}
   
   
   
}