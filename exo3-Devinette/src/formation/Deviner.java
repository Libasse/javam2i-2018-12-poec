package formation;

import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "deviner", eager = true)
@SessionScoped
public class Deviner {
   
	Random rnd = new Random ();
	int secret = rnd.nextInt(100);
	int rebours = 10;
	int saisie = -1;
	String message = "<<Bienvenue>>";
	
	public int getSaisie() {
		return saisie;
	}

	public void setSaisie(int saisie) {
		this.saisie = saisie;
	}
	
	public String getMessage() {
		return message;
	}

	public int getRebours() {
		return rebours;
	}
	
	public void valider ()
	{
		rebours--;
		if (saisie > secret) message ="Trop grand";
		else if (saisie < secret) message ="Trop petit";
		else message ="Bravo ! gagn� en " + (10-rebours) + "coup(s)";
		
		if (rebours == 0) 
		{
			message = "Dommage ! secret = " + secret;
			reset();
		}
		
	}
	
	private void reset()
	{
		rebours = 10;
		secret = rnd.nextInt(100);
	}
	
}