package formation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "compteur", eager = true)
@SessionScoped
public class Compteur {
	
   private int compteur = 0;

		public int getCompteur() {
			return compteur++;
		}
   
   
}