const webpack = require("webpack");
const path = require("path");



let config = {
  entry: ['js/SpaceCraft.js',
      'js/Ovni.js',
      'js/Rocket.js',
      'js/SpaceShip.js',
      'js/Game.js'
      
  ],
  output: {
    path: path.resolve(__dirname, "./public"),
    filename:  "/.main.js"
  }
}

module.exports = config;

