class SpaceCraft {
    constructor(src, x, y, width, height) {
        this.src = src;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.destroyed = false;
        this.fired = false;


    }

    move(dx, dy) {
        this.x += dx;
        this.y += dy;

    }

    draw() {
        let img = new Image();
        img.src = this.src;
        return img;
    }

    explode() {
        this.src = 'img/explode.png';
        this.destroyed = true;
        document.querySelector('#explode').play();

    }

    hit(target) {
        if (((this.x + this.width) >= target.x) && (target.x + target.width >= this.x)
                &&
                ((this.y + this.height) >= target.y) && (target.y + target.height) >= this.y) {

            target.explode();
            this.src = 'img/blank.png';
            return true;
        }
        return false;

    }

}


