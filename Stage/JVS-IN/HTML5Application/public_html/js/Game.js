class Game {
    /*
     * 
     * @param {string} idCanvas : l'identifiant de l'élément canvas
     * @param {string} idProgress :l'identifiant du parent de l'élément progress
     * @param {string} idScore:identifiant du calque contenant le score
     * @param {number} point : nombre de points au départ 
     * @param {number} nombre_ovnis: nombre d'ovnis ennemis
     * @returns {Game} le jeu
     */
    constructor(idCanvas, idProgress, idScore, point, nombre_ovnis) {
        this.canvas = document.querySelector(idCanvas);
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.context = this.canvas.getContext('2d');
        this.ovnis = [];
        //let nombre_ovnis = 4;
        let pas = (this.width - 400) / (nombre_ovnis + 1);
        for (let i = 0; i < nombre_ovnis; i++) {
            let x = (i + 1) * pas + i * 100;
            let y = Math.ceil(Math.random() * 50);
            let ovni = new Ovni(x, y);
            this.ovnis.push(ovni);
        }
        this.spaceShip = new SpaceShip(this.width / 2, this.height - 100);
        this.rockets = [];
        this.delayBeforeStarted = 100;
        this.interval = 0;
        this.speed = 25;
        
        this.point = point;
        this.score = document.querySelector(idScore);
        this.progress = document.querySelector(idProgress);
    }


    
    stop(state) {
        if (state) {
            var message = "Vous avez gagné !";
            this.context.fillStyle = "green";
        } else {
            var message = "Vous avez perdu !";
            this.context.fillStyle = "red";
        }
        clearInterval(this.interval);
        this.context.font = "50px Verdana"
        this.context.fillText(message, this.width / 2 - 200, this.height / 2);
        //this.finished.style.display = 'block';
    }

    display() {
        this.score.innerHTML = this.point;
        if (this.point === 0) {
            this.stop();
        }
    }

    load() {
        if (this.delayBeforeStarted === 0) {
            this.progress.style.display = 'none';
            this.canvas.style.display = 'block';
            this.interval = setInterval(function () {
                this.play();
            }.bind(this), 100);

        } else {
            this.delayBeforeStarted -= 10;
            this.progress.querySelector('progress').value = 100 - this.delayBeforeStarted;
            setTimeout(function () {
                this.load();
            }.bind(this), 100);
        }
    }

/*
 * 
 */
    play() {
        let fond = new Image();
        fond.src = 'img/univers.jpg';
        this.context.drawImage(fond, 0, 0, this.width, this.height);
        this.context.drawImage(this.spaceShip.draw(),
                this.spaceShip.x, this.spaceShip.y,
                this.spaceShip.width, this.spaceShip.height);

        if (this.spaceShip.destroyed) {
            setTimeout(function () {
                this.stop(false);
            }.bind(this), 100);
        } else if (this.ovnis.length === 0) {
            setTimeout(function () {
                this.stop(true);
            }.bind(this), 100);
        }
        this.ovnis.forEach(function (ovni, index) {
            ovni.move(0, 10);
            this.context.drawImage(ovni.draw(),
                    ovni.x, ovni.y, ovni.width, ovni.height);

            if (ovni.y > this.height) {
                ovni.y = 0;
                if (!ovni.destroyed) {
                    this.point--;
                } else {
                    this.ovnis.splice(index, 1);
                }

            }
            if (ovni.x < 0)
                ovni.x = 0;
            if (ovni.x > this.width - ovni.width)
                ovni.x = this.width - ovni.width;

            if (!ovni.destroyed)
                ovni.hit(this.spaceShip);

            //if (this.rocket !== null) {
            this.rockets.forEach(function (rocket, index) {
                this.context.drawImage(rocket.draw(),
                        rocket.x, rocket.y,
                        rocket.width, rocket.height);

                if (rocket.hit(ovni)) {
                    this.point++;
                    this.rockets.splice(index, 1);
                } else {
                    rocket.move(0, -10);
                    if (rocket.y < 0) {
                        this.rockets.splice(index, 1);
                    }
                }
            }.bind(this));

        }.bind(this));
        this.display();
    }

    move(e) {
        var key = e.key || e.keyCode;
        switch (key) {
            case 'ArrowRight': // Déplacement vers la droite
                this.spaceShip.move(this.speed, 0);
                if (this.spaceShip.x > this.width)
                    this.spaceShip.x = 0;
                break;
            case 'ArrowLeft': // Déplacement vers la gauche
                this.spaceShip.move(-this.speed, 0);
                if (this.spaceShip.x < 0)
                    this.spaceShip.x = this.width;
                break;
            case 'ArrowDown': // Déplacement vers le bas
                this.spaceShip.move(0, this.speed);
                if (this.spaceShip.y > this.height)
                    this.spaceShip.y = 0;
                break;
            case 'ArrowUp': // Déplacement vers le haut
                this.spaceShip.move(0, -this.speed);
                if (this.spaceShip.y < 0)
                    this.spaceShip.y = this.height;
                break;
            case '+': // Augmentation de la vitesse de  (max : 75)
                this.speed += 5;
                this.speed = Math.min(this.speed, 75);
                break;
            case '-': // Diminution de la vitesse de déplacement (min : 5)
                this.speed -= 5;
                this.speed = Math.max(this.speed, 5);
                break;
            case ' ': // Lance un missile
                this.rockets.push(this.spaceShip.fire());               
                break;
        }
    }
}




