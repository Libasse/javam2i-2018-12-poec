/*****************************************************************
 * Auteur : Serge L'HOSTIS
 * Date   : 2018-09-29
 * Description : Construction d'un menu principal d'un site Web
 * References : 
 *      bootstrap.css (version 4)
 *      bootstrap.js  (version 4)
 *      jquery.js     (toute version)
 *      fichier JSON  contenant les données du menu
 *****************************************************************/
var Menu = {}; // Espace de nom pour le menu

/**
 * Construit le menu principal
 * @param {string} filename : nom du fichier JSON
 * @param {string} id : identifiant du container du menu : #id, elem, .cssClass
  */
Menu.load = function(filename, id) {
    var $menu = $(id);
    $menu.attr({"class":"navbar navbar-expand-lg navbar-light bg-light"});
    
    $.getJSON(filename, function (json) {
        for (var key in json) {
            if (key === 'title') {
                Menu.title($menu, json, 'title');
            } else if (key === 'data') {
                
                $container = $('<div class="collapse navbar-collapse" id="navbar">').appendTo($menu);
                $ul = Menu.mainMenu($container);
                
                for (var subKey in json[key]) {
                    var value = json[key][subKey];
                    if (typeof (value) === 'string') {
                        Menu.singleMenu($ul, subKey, value);
                    } else {
                        $subUL = Menu.dropdownMenu($ul, subKey);
                        Menu.subMenu($subUL, value);
                    }
                }
            }
        }
    });
};

/**
 * Constuit un menu sans sous-menu 
 * @param {jQuery} $container 
 * @param {string} label : libellé du menu
 * @param {string} url : adresse du fichier
  */
Menu.singleMenu = function($container, label, url) {
    $('<li class="nav-item active">' 
           + '<a href="' + url + '" class="nav-link">' 
           + label + '</a></li>').appendTo($container);
};

/**
 * Construit un container de sous-menu
 * @param {jQuery} $container : container du sous-menu (le menu principal)
 * @param {string} label : libellé du menu
 * @returns {jQuery} : le sous-menu
 */
Menu.dropdownMenu = function($container, label) {
    var $dropdown = $('<li class="nav-item dropdown">').appendTo($container);
    $('<a href="?" class="nav-link dropdown-toggle" data-toggle="dropdown" '
        + 'role="button" aria-expanded="false">'
        + label + '<span class="caret"></span></a>').appendTo($dropdown);
    var $ul = $('<div class="dropdown-menu" role="menu">').appendTo($dropdown);
    return $ul;
};

/**
 * Construit un sous-menu
 * @param {jQuery} $container : container du sous-menu (le dropdown)
 * @param {JSON} json : tableau JSON représentant le sous-menu
  */
Menu.subMenu = function($container, json) {
    for (var key in json) {
        var value = json[key];
        if (key.indexOf('divider') === -1)
            $('<a href="'+ value +'" class="dropdown-item">'+ key +'</a>')
                .appendTo($container);
        else
            $('<div class="dropdown-divider"></div>').appendTo($container);
    }
};

/**
 * Construit l'onglet à gauche du menu
 * @param {jQuery} $container : le menu
 * @param {JSON} json : le tableau JSON contenant le menu
 * @param {string} key : la clé du tableau JSON concernant le titre
  */
Menu.title = function($container, json, key) {
    var subJson = json[key];             
    for (var subkey in subJson) {
        var value = subJson[subkey].trim();
        $('<a class="navbar-brand" href="'+ value + '">' + subkey + '</a>')
                .appendTo($container);
        break; // Normalement, il n'y a qu'un titre
    }  
    var $button = $('<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" aria-label="Toggle navigation">').appendTo($container);
    $('<span class="navbar-toggler-icon"></span>').appendTo($button);
};

/**
 * Construit un élément du menu principal
 * @param {jQuery} $container : container de l'élément du menu principal
 * @returns {jQuery} : un élément du menu principal
 */
Menu.mainMenu = function($container) {
    var $ul = $('<ul class="mr_auto navbar-nav">').appendTo($container);
    return $ul;
};