class SpaceShip extends SpaceCraft {
    constructor (x,y) {
        super('img/spatialship.png',x,y,128,60);
    }
    
    fire() {
        this.fired=true;
        var rocket = new Rocket(this.x + this.width/2, this.y);
        rocket.y -= rocket.height;
        return rocket;       //new Rocket(this.x + this.width/2, this.y);
        
    }
}


