class Game {

    constructor() {

    }

    /**
     * Vérifie quelle est la touche appuyée lors de l'événement keydown du document
     * Lance une action sur la navette spatiale en fonction de la touche
     * @param {event} e : événement
     */
    move(e) {
        var key = e.key || e.keyCode;
        switch (key) {
            case 'ArrowRight': // Déplacement vers la droite
                this.spaceShip.move(this.speed, 0);
                break;
            case 'ArrowLeft': // Déplacement vers la gauche
                this.spaceShip.move(-this.speed, 0);
                break;
            case 'ArrowDown': // Déplacement vers le bas
                this.spaceShip.move(0, this.speed);
                break;
            case 'ArrowUp': // Déplacement vers le haut
                this.spaceShip.move(0, -this.speed);
                break;
            case '+': // Augmentation de la vitesse de  (max : 75)
                this.speed += 5;
                this.speed = Math.min(this.speed, 75);
                break;
            case '-': // Diminution de la vitesse de déplacement (min : 5)
                this.speed -= 5;
                this.speed = Math.max(this.speed, 5);
                break;
            case ' ': // Lance un missile
                if (!this.spaceShip.fired) {
                    this.rocket = this.spaceShip.fire();
                    this.spaceShip.fired = true;
                }
                break;
        }
    }
}


