/* jshint esversion: 6 */
import SpaceCraft from './SpaceCraft.js';

export class Rocket extends SpaceCraft {
    
    constructor( x, y) {
        super('img/rocket.png', x, y, 16, 48);
    }

    hit(target) {

    }
}