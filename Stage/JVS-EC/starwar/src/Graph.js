/* jshint esversion: 6 */
import SpaceCraft from './SpaceCraft.js';
import SpatialShip from './SpatialShip.js';
import Rocket from './Rocket.js';
import Ovni from './Ovni.js';

export class Graph {
    
    constructor(width, height) {
        this.src = src;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.hited = false;
        this.ovnis = [ new Ovni(50, 100), new Ovni(300, 50), new Ovni(600, 150), new Ovni(1000, 50) ];
        this.spatialShip = new SpatialShip(50, 600);
    }

    get width() { return this.width; }
    set width(value) { this.width = value; }

    get height() { return this.height; }
    set height(value) { this.height = value; }

    get canvas() { return this.canvas; }
    set canvas(value) { this.canvas = value; }

    get context() { return this.context; }
    set context(value) { this.context = value; }

    get ovnis() { return this.ovnis; }
    set ovnis(value) { this.ovnis = value; }

    get spatialShip() { return this.spatialShip; }
    set spatialShip(value) { this.spatialShip = value; }

    get rocket() { return this.rocket; }
    set rocket(value) { this.rocket = value; }

    init(id) {

    }

    draw() {

    }
}
