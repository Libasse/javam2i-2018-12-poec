/* jshint esversion: 6 */
import Graph from './Graph.js';
import Game from './Game.js';
graph = new Graph();
graph.init('canvas');
game = new Game();
game.load();
window.addEventListener("keydown", game.move, false);