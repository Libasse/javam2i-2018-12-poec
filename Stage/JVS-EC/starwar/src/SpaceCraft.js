/* jshint esversion: 6 */
export class SpaceCraft {
    
    constructor(src, x, y, width, height) {
        this.src = src;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.hited = false;
    }

    get src() { return this.src; }
    set src(value) { this.src = value; }

    get x() { return this.x; }
    set x(value) { this.x = value; }

    get y() { return this.y; }    
    set y(value) { this.y = value; }

    get width() { return this.width; }
    set width(value) { this.width = value; }

    get height() { return this.height; }
    set height(value) { this.height = value; }

    get hited() { return this.hited; }
    set hited(value) { this.height = value; }

    move(dx, dy) {

    }

    draw() {

    }

    explode() {

    }

}
