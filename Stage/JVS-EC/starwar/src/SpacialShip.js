/* jshint esversion: 6 */
import SpaceCraft from './SpaceCraft.js';

export class SpatialShip extends SpaceCraft {
    
    constructor( x, y) {
        super('img/spatialship.png', x, y, 128, 60);
        this.fired = false;
    }

    get fired() { return this.fired; }
    set fired(value) { this.fired = value; }

    fire() {
        this.fired = true;
    }
}