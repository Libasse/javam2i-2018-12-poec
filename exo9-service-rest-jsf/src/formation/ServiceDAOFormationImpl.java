package formation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ServiceDAOFormationImpl implements ServiceDAOFormation {
	Connection conn = null;
	
	public ServiceDAOFormationImpl() throws Exception {
		  String myDriver = "com.mysql.cj.jdbc.Driver";
	      String myUrl = "jdbc:mysql://localhost/formationdb?useLegacyDatetimeCode=false&serverTimezone=Europe/Paris";
	      Class.forName(myDriver);
	      conn = DriverManager.getConnection(myUrl, "root", "rootroot");	      
	}
	
	@Override
	public void ajouter(Formation f) throws Exception {
		PreparedStatement dStmt = conn.prepareStatement("insert into formations (libelle) values (?)");
		dStmt.setString (1, f.getLibelle());
		dStmt.execute();
	}

	@Override
	public List<Formation> lister() throws Exception {
		PreparedStatement dStmt = conn.prepareStatement("select * from formations");
		List<Formation>formations = new ArrayList<Formation>();
		ResultSet rs = dStmt.executeQuery();
		while (rs.next()) {
			Formation formation = new Formation();
		    formation.setId(rs.getInt(1));
		    formation.setLibelle(rs.getString(2));
		    formations.add(formation);
		 }
		return formations;
	}

}
