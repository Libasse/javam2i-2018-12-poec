package formation;

import java.io.Serializable;

/*import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;*/

//@PersistenceCapable (identityType=IdentityType.APPLICATION)
public class Formation implements Serializable{
	
	//@PrimaryKey
	//@Persistent
	int id ;
	//@Persistent
	String libelle;

	public Formation ()
	{
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public Formation (int id, String libelle)
	{
		this.id = id;
		this.libelle = libelle;
	}
	
	@Override
	public String toString ()
	{
		return String.format ("%d -- %s", id, libelle);
	}
}
