package formation;

import java.util.List;

public interface ServiceDAOFormation {
	void ajouter (Formation f)  throws Exception;
	List<Formation>lister ()  throws Exception;
}
