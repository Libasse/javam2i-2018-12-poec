package formation;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/daoformations")
public class DAOServiceRest {
	
	@RequestMapping (value="/{id}", method=RequestMethod.GET, produces = "application/json")
	public String lireFormation (@PathVariable int id)
	{		
		return "";
	}
	
	@RequestMapping (value="/", method=RequestMethod.GET, produces = "application/json")
	public String lireFormations ()
	{		
		return "";
	}
	
	@RequestMapping (value="/", method=RequestMethod.POST, consumes = "application/json")
	public void ajouterFormation (Formation f)
	{		
		return "";
	}
	
}