package com.example.myapplication6;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {
TextView txtcompteur = null;
Handler h = new Handler();
int compteur =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);


        Button ok = findViewById(R.id.bok);
        txtcompteur = findViewById(R.id.txtcompteur);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               new Thread(new Runnable() {
                   @Override
                   public void run() {
                       while (true)
                       {
                           h.post(new Runnable() {
                               @Override
                               public void run() {
                                   compteur = Integer.parseInt(txtcompteur.getText().toString());
                                   compteur++;
                                   txtcompteur.setText(String.valueOf(compteur));
                               }
                           });
                           try {
                               Thread.sleep(100);
                           } catch (InterruptedException e) {
                               e.printStackTrace();
                           }

                       }
                   }
               }).start();
               // Thread de communication socket 2222
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ServerSocket s = new ServerSocket(2222);
                            Socket socketClient = s.accept();
                            OutputStream out = socketClient.getOutputStream();
                            while (true) {
                                // ecoute du socket
                                out.write((String.valueOf(compteur) + "\r\n").getBytes());
                                Thread.sleep(100);
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }).start();
    }
            });
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
