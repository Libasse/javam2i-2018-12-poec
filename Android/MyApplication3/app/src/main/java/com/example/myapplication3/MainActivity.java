package com.example.myapplication3;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    Drawable [] images= new Drawable[4];
    ImageView iv;
    Handler h = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = findViewById(R.id.demarre);
        iv = findViewById(R.id.animation);
        // initialiser tableau d'images
        for (int i=0;i<4;i++)
        {
           int id  = getResources().getIdentifier("@drawable/animation" + (i+1),null,this.getPackageName());
           images[i]=getResources().getDrawable(id);
        }
        b.setOnClickListener(new View.OnClickListener() {
            int compteur=0;
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (true)
                        {
                            h.post(new Runnable() {
                                @Override
                                public void run() {
                                    iv.setImageDrawable(images[compteur]);
                                    compteur++;
                                    if (compteur >=4) compteur=0;
                                }
                            });
                            // afficher la nouvelle image


                            try {
                                Thread.sleep(1000/8);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
