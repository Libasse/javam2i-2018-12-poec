
package rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour cplanete complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="cplanete">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coordx" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="coordy" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diametre" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="distance" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nbsat" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cplanete", propOrder = {
    "coordx",
    "coordy",
    "diametre",
    "distance",
    "nbsat",
    "nom"
})
public class Cplanete {

	protected String nom;
    protected int coordx;
    protected int coordy;
    protected int diametre;
    protected int distance;
    protected int nbsat;
   

    

	/**
     * Obtient la valeur de la propri�t� coordx.
     * 
     */
    
    
    
    
    


	public int getCoordx() {
        return coordx;
    }
	

	public Cplanete(String nom, int coordx, int coordy, int diametre, int distance, int nbsat) {
		super();
		this.nom = nom;
		this.coordx = coordx;
		this.coordy = coordy;
		this.diametre = diametre;
		this.distance = distance;
		this.nbsat = nbsat;
	}


	/**
     * D�finit la valeur de la propri�t� coordx.
     * 
     */
    public void setCoordx(int value) {
        this.coordx = value;
    }

    /**
     * Obtient la valeur de la propri�t� coordy.
     * 
     */
    public int getCoordy() {
        return coordy;
    }

    /**
     * D�finit la valeur de la propri�t� coordy.
     * 
     */
    public void setCoordy(int value) {
        this.coordy = value;
    }

    /**
     * Obtient la valeur de la propri�t� diametre.
     * 
     */
    public int getDiametre() {
        return diametre;
    }

    /**
     * D�finit la valeur de la propri�t� diametre.
     * 
     */
    public void setDiametre(int value) {
        this.diametre = value;
    }

    /**
     * Obtient la valeur de la propri�t� distance.
     * 
     */
    public int getDistance() {
        return distance;
    }

    /**
     * D�finit la valeur de la propri�t� distance.
     * 
     */
    public void setDistance(int value) {
        this.distance = value;
    }

    /**
     * Obtient la valeur de la propri�t� nbsat.
     * 
     */
    public int getNbsat() {
        return nbsat;
    }

    /**
     * D�finit la valeur de la propri�t� nbsat.
     * 
     */
    public void setNbsat(int value) {
        this.nbsat = value;
    }

    /**
     * Obtient la valeur de la propri�t� nom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom() {
        return nom;
    }

    /**
     * D�finit la valeur de la propri�t� nom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom(String value) {
        this.nom = value;
    }

}
