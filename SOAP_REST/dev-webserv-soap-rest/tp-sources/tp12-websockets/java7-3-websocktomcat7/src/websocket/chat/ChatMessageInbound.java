package websocket.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;


import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.WsOutbound;


public final class ChatMessageInbound extends MessageInbound {
    private static String GUEST_PREFIX = "Astronome num�ro ";
    private final String nickname;
    private final ChatWebSocketServlet serv;

    public ChatMessageInbound(int id,ChatWebSocketServlet serv) {
        this.nickname = GUEST_PREFIX + id;
        this.serv=serv;      
    }
    @Override
    protected void onOpen(WsOutbound outbound) {
        serv.connections.add(this);
        String message = String.format("* %s %s",
                nickname, "vient de se connecter");
        broadcast(message);
    }
    @Override
    protected void onClose(int status) {
        serv.connections.remove(this);
        String message = String.format("* %s %s",
                nickname, " s'est d�connect�");
        broadcast(message);
    }

    @Override
    protected void onBinaryMessage(ByteBuffer message) throws IOException {
        throw new UnsupportedOperationException(
                "Binary message non support�");
    }

    @Override
    protected void onTextMessage(CharBuffer message) throws IOException {
        // Never trust the client
        String filteredMessage = String.format("%s: %s",
                nickname, message.toString());
        broadcast(filteredMessage);
    }

    private void broadcast(String message) {
    
        for (ChatMessageInbound connection : serv.connections) {
            try {
            	CharBuffer buffer1=null;
            	 if (message.contains("exoplanete")){
            		   Path resultat = FileSystems.getDefault().getPath("C:/bigdataastrophysique/global/resultats.txt");
            		    try(BufferedReader reader = Files.newBufferedReader(  
            		     resultat, Charset.defaultCharset())){  
            		     String lineFromFile = ""; 
            		      while((lineFromFile = reader.readLine()) != null){  	
            		    	  buffer1 =CharBuffer.wrap(lineFromFile);
                              connection.getWsOutbound().writeTextMessage(buffer1);
            		       } 
            		    }catch(IOException exception){  
            		    	System.out.println(exception.getMessage());
            		    }   
                     }
                CharBuffer buffer = CharBuffer.wrap(message);
                connection.getWsOutbound().writeTextMessage(buffer);
            } catch (IOException ignore) {
                // Ignore
            }
        }
    }
}