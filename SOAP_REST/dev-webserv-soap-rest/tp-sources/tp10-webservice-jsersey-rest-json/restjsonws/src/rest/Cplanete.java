package rest;
public class Cplanete {
  private String nom;
  private int distance, diametre,nbsat,coordx,coordy;
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public int getDistance() {
	return distance;
}
public Cplanete(String nom, int distance, int diametre, int nbsat, int coordx,
		int coordy) {
	super();
	this.nom = nom;
	this.distance = distance;
	this.diametre = diametre;
	this.nbsat = nbsat;
	this.coordx = coordx;
	this.coordy = coordy;
}
public void setDistance(int distance) {
	this.distance = distance;
}
public int getDiametre() {
	return diametre;
}
public void setDiametre(int diametre) {
	this.diametre = diametre;
}
public int getNbsat() {
	return nbsat;
}
public void setNbsat(int nbsat) {
	this.nbsat = nbsat;
}
public int getCoordx() {
	return coordx;
}
public void setCoordx(int coordx) {
	this.coordx = coordx;
}
public int getCoordy() {
	return coordy;
}
public void setCoordy(int coordy) {
	this.coordy = coordy;
}

}
  