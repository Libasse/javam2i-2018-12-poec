package rest;
import java.util.*;
public class PlaneteSimpleMap 
       {
  private Map<String,Cplanete> planetes;

  public PlaneteSimpleMap() {
    planetes = new HashMap<String,Cplanete>();
    addPlanete(new Cplanete("mercure",50,6000,0,0,0));
    addPlanete(new Cplanete("venus",100,6000,0,0,0));
    addPlanete(new Cplanete("terre",150,12000,1,0,0));
    addPlanete(new Cplanete("mars",300,6000,2,0,0));
    addPlanete(new Cplanete("jupiter",1000,100000,4,0,0));
  }
  public Cplanete findPlanete(String nom) {  
	  Cplanete x = planetes.get(nom.toLowerCase());
	  if (x!=null) {
		  return  x ;
   		}
	
		  return new Cplanete("inconnue",0,0,0,0,0);
	 
  }

  private void addPlanete(Cplanete planete) {
    planetes.put(planete.getNom(),planete);
  }
}
