package rest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response; 
import org.json.simple.JSONObject;
@Path("/")
// http://localhost:8080/sullywebajax/api/restjsonws/mercure
//pour la plan�te jupiter par exemple
public class PlaneteRest
{ 
	//si l'utilisateur saisit /restjsonws alors appeler getPlanete
	@GET   
	@Path("/restjsonws/{nom}") 
	@Consumes(MediaType.APPLICATION_JSON)  
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPlanete(@PathParam("nom") String nom)
	{ 
		PlaneteSimpleMap psm = new PlaneteSimpleMap();
		Cplanete pl = psm.findPlanete(nom);
		JSONObject obj=new JSONObject();
		obj.put("nom",pl.getNom()); 
		obj.put("distance",pl.getDistance());
		obj.put("diametre",pl.getDiametre()); 
	//	obj.put("nbsat", pl.getNbsat());
		StringBuilder builder = 
				new StringBuilder(obj.toString()); 
		return Response.status(200).entity(builder.toString()).build();    
	}
}

