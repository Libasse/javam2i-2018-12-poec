package ws;
import javax.jws.*; 
import javax.jws.soap.SOAPBinding;
@WebService(name="Planetes") 
@SOAPBinding(style = SOAPBinding.Style.RPC) 
public class MyWebServices 
{ 
  @WebMethod(operationName="getCoord") 
  @WebResult(name = "planete") 
  public  Cplanete getCoord(String nom) 
  { 
			PlaneteSimpleMap psm = new PlaneteSimpleMap();
			Cplanete pl = psm.findPlanete(nom);			
			return pl;
  } 
}

