
package demo.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cplanete complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cplanete">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coordx" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="coordy" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="diametre" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="distance" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nbsat" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cplanete", propOrder = {
    "coordx",
    "coordy",
    "diametre",
    "distance",
    "nbsat",
    "nom"
})
public class Cplanete {

    protected int coordx;
    protected int coordy;
    protected int diametre;
    protected int distance;
    protected int nbsat;
    protected String nom;

    /**
     * Gets the value of the coordx property.
     * 
     */
    public int getCoordx() {
        return coordx;
    }

    /**
     * Sets the value of the coordx property.
     * 
     */
    public void setCoordx(int value) {
        this.coordx = value;
    }

    /**
     * Gets the value of the coordy property.
     * 
     */
    public int getCoordy() {
        return coordy;
    }

    /**
     * Sets the value of the coordy property.
     * 
     */
    public void setCoordy(int value) {
        this.coordy = value;
    }

    /**
     * Gets the value of the diametre property.
     * 
     */
    public int getDiametre() {
        return diametre;
    }

    /**
     * Sets the value of the diametre property.
     * 
     */
    public void setDiametre(int value) {
        this.diametre = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     */
    public int getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     */
    public void setDistance(int value) {
        this.distance = value;
    }

    /**
     * Gets the value of the nbsat property.
     * 
     */
    public int getNbsat() {
        return nbsat;
    }

    /**
     * Sets the value of the nbsat property.
     * 
     */
    public void setNbsat(int value) {
        this.nbsat = value;
    }

    /**
     * Gets the value of the nom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets the value of the nom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom(String value) {
        this.nom = value;
    }

}
