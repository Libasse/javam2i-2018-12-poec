<?xml version="1.0" encoding="iso-8859-1" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
			<xsl:apply-templates select="LISTEDESCEPAGES"/>
	</xsl:template>
	
	
	
	<xsl:template match="LISTEDESCEPAGES">
		<xsl:element name="VINS">
			<xsl:attribute name="NOMBRE">
		 	<xsl:value-of select="count(//CEPAGE)"/>
			</xsl:attribute>
		<xsl:apply-templates select="CEPAGE"/>
		</xsl:element>
	</xsl:template>
	
	
	
	<xsl:template match="CEPAGE">
		<xsl:copy>
		<xsl:attribute name="QUALITE">
		<xsl:value-of select="@TYPE"/>
		</xsl:attribute>
		<xsl:value-of select="@NOM"/>
			<xsl:apply-templates select="APPELLATION"/>
		</xsl:copy>

	</xsl:template>
	
	<xsl:template match="APPELLATION">

		<xsl:element name="PROVENANCE">
		<xsl:apply-templates/>
			<xsl:value-of select="@NOM"/>
		</xsl:element>	
	</xsl:template>
	
	
	<xsl:template match="ANNEE">
		<xsl:element name="MILLESIME">
			<xsl:attribute name="DEGRE">
		
				<xsl:number value="@DENSITE div 17"/>
				
			</xsl:attribute>
			<xsl:value-of select="@DATE"/>
		</xsl:element>
	</xsl:template>
	
	
</xsl:stylesheet>

