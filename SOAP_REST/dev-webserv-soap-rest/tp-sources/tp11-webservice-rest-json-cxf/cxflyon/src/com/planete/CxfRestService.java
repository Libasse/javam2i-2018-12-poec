package com.planete;
//TOP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//http://localhost:8080/cxflyon/rest/planeteservices/getplanetedetail?planetenom=mars
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@WebService(name="planeteService", targetNamespace="http://www.lessonslab.com/cxf-rest/example")
public interface CxfRestService 
{
	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	@Path("/getplanetedetail")
	public Response getPlaneteDetail(@QueryParam("planetenom") String planeteNom);
	
}
