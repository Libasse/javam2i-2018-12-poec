package com.planete;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="planete")

public class PlaneteBean 
{
	private String nom;
	private Integer distance;
	private Integer diametre;
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Integer getDistance() {
		return distance;
	}
	public void setDistance(Integer distance) {
		this.distance = distance;
	}
	public Integer getDiametre() {
		return diametre;
	}
	public void setDiametre(Integer diametre) {
		this.diametre = diametre;
	}
	
}
