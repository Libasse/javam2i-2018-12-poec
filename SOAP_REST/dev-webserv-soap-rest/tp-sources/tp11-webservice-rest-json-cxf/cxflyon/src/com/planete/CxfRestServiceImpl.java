package com.planete;

import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
public class CxfRestServiceImpl implements CxfRestService 
{
	@Autowired
	private PlaneteDao planeteDao; 

	
	@Override
	public Response getPlaneteDetail(String planeteNom) {
		// TODO Auto-generated method stub
		{
			if(planeteNom == null)
			{
				return Response.status(Response.Status.BAD_REQUEST).build();
			}		
			return Response.ok(planeteDao.getPlaneteDetails(planeteNom)).build();
	}
}
}