package clientcxfsoapweb1;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import service.BookShelfService;
import vo.BookVO;


public class TestCxfSoapWeb1 {
	public static void main(String[] args) {
	       String serviceUrl = "http://localhost:8080/cxfsoapweb1/bookshelfservice";
	               JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();

	               factory.setServiceClass(BookShelfService.class);
	               factory.setAddress(serviceUrl);
	               BookShelfService bookService = (BookShelfService) factory.create();
	        
	               //insert book
	               BookVO bookVO = new BookVO();
	               bookVO.setAuthor("Issac Asimov");
	               bookVO.setBookName("Foundation and Earth");
	               String result = bookService.insertBook(bookVO);
	               System.out.println("result : " + result);
	               bookVO = new BookVO();
	               bookVO.setAuthor("Issac Asimov");
	               bookVO.setBookName("Foundation and Empire");
	        
	               result = bookService.insertBook(bookVO);
	        
	               System.out.println("result : " + result);
	        
	               bookVO = new BookVO();
	               bookVO.setAuthor("Arthur C Clarke");
	               bookVO.setBookName("Rama Revealed");
	        
	               result = bookService.insertBook(bookVO);
	        
	               System.out.println("result : " + result);
	        
	               //retrieve book
	               bookVO = bookService.getBook("Foundation and Earth");
	        
	               System.out.println("book name : " + bookVO.getBookName());
	               System.out.println("book author : " + bookVO.getAuthor());
	}
}
