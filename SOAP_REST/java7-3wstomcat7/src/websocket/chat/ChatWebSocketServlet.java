package websocket.chat;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;

/**
 * Servlet implementation class ChatWebSocketServlet
 */
@WebServlet("/ChatWebSocketServlet")
public class ChatWebSocketServlet extends WebSocketServlet {
	private static final long serialVersionUID = 1L;
       
	 private Integer connectionIds = new Integer(0);
	    //Tableau des connexions
	    public final Set<ChatMessageInbound> connections =
	            new CopyOnWriteArraySet<ChatMessageInbound>();
		@Override
	    protected StreamInbound createWebSocketInbound(String subProtocol,
	            HttpServletRequest request) {
	    	//createWebSocketInbound appel� � chaque connexion utilisateur
	    	Integer toto=connectionIds++;
	    	StreamInbound s = new ChatMessageInbound(toto,this);
	        return s;      
	    }

}
