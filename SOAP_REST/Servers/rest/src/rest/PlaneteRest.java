package rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONObject;
@Path("/")
public class PlaneteRest {

	@GET   
	@Path("/restjsonws/{nom}") 
	@Consumes(MediaType.APPLICATION_JSON)  
	@Produces(MediaType.APPLICATION_JSON)
		public Response getPlanete(@PathParam("nom") String nom)
	{ 
		PlaneteSimpleMap psm = new PlaneteSimpleMap();
		Cplanete pl = psm.findPlanete(nom);
		JSONObject obj=new JSONObject();
		obj.put("nom",pl.getNom()); 
		obj.put("distance",pl.getDistance());
		obj.put("diametre",pl.getDiametre()); 
		JSONObject objext=new JSONObject();
		objext.put("planete", obj);
		StringBuilder builder = 
				new StringBuilder(objext.toString()); 
		return Response.status(200).entity(builder.toString()).build();    
		}

}
