package ws;

import java.util.HashMap;
import java.util.Map;

public class PlaneteSimpleMap {
	
	private Map<String, Cplanete> planetes;
	
	public PlaneteSimpleMap() {
		planetes = new HashMap<String, Cplanete>();
		addPlanete (new Cplanete("mercure", 50, 6000,0,0,0));
		addPlanete (new Cplanete("venus", 50, 6000,0,0,0));
		addPlanete (new Cplanete("terre", 50, 6000,0,0,0));
		addPlanete (new Cplanete("mars", 50, 6000,0,0,0));
		addPlanete (new Cplanete("jupiter", 50, 6000,0,0,0));
	}
	
	  public Cplanete findPlanete(String nom) {  
		  Cplanete x = planetes.get(nom.toLowerCase());
		  if (x!=null) {
			  return  x ;
	   		}
		
			  return new Cplanete("inconnue",0,0,0,0,0);
		 
	  }

	  private void addPlanete(Cplanete planete) {
	    planetes.put(planete.getNom(),planete);
	  }


}
