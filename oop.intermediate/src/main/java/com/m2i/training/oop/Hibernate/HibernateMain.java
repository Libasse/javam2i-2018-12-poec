package com.m2i.training.oop.Hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateMain {

	public static void main(String[] args) {
		// Obtenir une SessionFactory
		SessionFactory sessionFactory=null;
		
		// A SessionFactory is set up once for an application!
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure() // configures settings from hibernate.cfg.xml
				.build();
		try {
			sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
		}
		catch (Exception e) {
			// The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
			// so destroy it manually.
			//e.printStackTrace();
			StandardServiceRegistryBuilder.destroy( registry );
			System.exit(1);
		}
		
		
		// obtenir une session à partir de SessionFactory
		Session session= sessionFactory.openSession();
		// Démarer une transaction
		session.beginTransaction();
		
		// effectuer des requêtes
		Film f=session.find(Film.class, new Long(1));
		System.out.println(f);
		
		// Commiter une transaction
		session.getTransaction().commit();
		// Fermer la session
		session.close();
		sessionFactory.close();

	}

}
