package com.m2i.training.oop.Hibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.m2i.training.oop.Hibernate.Film; 

public class FilmMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
EntityManagerFactory entityManagerFactory =
Persistence.createEntityManagerFactory("pu1");
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();	
		
		Film f = entityManager.find(Film.class, new Long(1));		
		System.out.println(f);
		
		System.out.println(f.getActors());
		
		entityManager.getTransaction().commit();
		entityManager.close();
		entityManagerFactory.close();

	}

}
