package com.m2i.training.oop.Hibernate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
public class Customer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="customer_id")
	private Long id;
	
	@Column(name="store_id")
	private Long storeId;
	
	@Column(name="first_name")
	private String firstname;
	
	@Column(name="last_name")
	private String lastname;
	
//	@Column(name="address_id")
//	private Long addressedId;
	
	
	@ManyToOne
	@JoinColumn(name="address_id")
	private Address address;
	
	@Column(name="create_date")	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoredId(Long storedId) {
		this.storeId = storedId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

//	public Long getAddressedId() {
//		return addressedId;
//	}
//
//	public void setAddressedId(Long addressedId) {
//		this.addressedId = addressedId;
//	}
	
	


	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	
	@Override
	public String toString() {
		return "Customer [id=" + id + ", storeId=" + storeId + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", address=" + address + ", createDate=" + createDate + "]";
	}
	
	
	
}
