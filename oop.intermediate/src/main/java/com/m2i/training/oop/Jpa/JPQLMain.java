package com.m2i.training.oop.Jpa;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

//import com.m2i.training.javase.oop.intermediate.sakila.Film;
import com.m2i.training.oop.Hibernate.Film;

public class JPQLMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory entityManagerFactory =
				Persistence.createEntityManagerFactory("pu1");
		
		EntityManager entityManager =
				entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		
		// TODO
		String qlString = "SELECT f FROM Film f"; // different d'une requête SQL
		TypedQuery<Film> query=
				entityManager.createQuery(qlString, Film.class);
		query.setFirstResult(10);
		query.setMaxResults(50);
		List<Film> films =  query.getResultList();
		System.out.println(films);
		
		
		 qlString = "SELECT f FROM Film f WHERE f.id < 500";		
		 query=
				entityManager.createQuery(qlString, Film.class);		
		 films =  query.getResultList();
		 System.out.println(films);
		 
		 
		 qlString = "SELECT f FROM Film f WHERE f.id < :data";			
		 query=
				entityManager.createQuery(qlString, Film.class);
		 query.setParameter("data", new Long(150));		
		 films =  query.getResultList();
		 System.out.println(films);
		 
		 
		 // selectionner le film d'id 1
		 qlString = "SELECT f FROM Film f WHERE f.id = :data";			
		 query=
				entityManager.createQuery(qlString, Film.class);
		 query.setParameter("data", new Long(1));		
		 Film f1 =  query.getSingleResult();		 
		 System.out.println(f1);
		 
		 
		 // Jointures
		// qlString = "SELECT f FROM Film f JOIN f.actors a on a.id=1";
		 //qlString = "SELECT f FROM Film f JOIN FETCH f.actors a";
		 query=
					entityManager.createQuery(qlString, Film.class);
		 films =  query.getResultList();
		 
		 for (Film f:films) {
			 System.out.println(f.getActors());
		 }
		 //System.out.println(films);
		 
		 
		 
		 entityManager.getTransaction().commit();
		 entityManager.close();
		 entityManagerFactory.close();
		 
	}
	
}
		 
		 
		 
		 
		 
