package com.m2i.training.javase.oop.intermediate.sakila;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/* Disclaimer: ce n'est pas un vrai repository comme vous 
	 * auriez dans spring Framework, ou ce 
	 
	 */
public class FilmRepository {
	
	public List<Film> findAll(){
		

		List<Film> films = new ArrayList<Film>();
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?serverTimezone=UTC","root","rootroot")) {
			
			
			Statement  stmt = con.createStatement();
			ResultSet rs=stmt.executeQuery("SELECT * FROM film");
			while (rs.next()) {
				//System.out.println(rs.getString("title"));
				Film film = new Film(rs.getInt("film_id"),
									rs.getString("title"),
									rs.getString("description"),
									rs.getString("release_year"));
						films.add(film);
			}
			
			
				
			} catch (SQLException e) {
				
				//e.printStackTrace();
				throw new RuntimeException(e);
			}
		return films;
	}

	public Film find(int id) {
try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?serverTimezone=UTC","root","rootroot")) {
			
			
			PreparedStatement  stmt = con.prepareStatement("SELECT * FROM film WHERE film_id= ?");
			//ResultSet rs=stmt.executeQuery("SELECT * FROM film WHERE film_id= ?");
			
			stmt.setInt(1, id);
			ResultSet rs=stmt.executeQuery();
			if (rs.next()) {
				Film film = new Film(rs.getInt("film_id"),
						rs.getString("title"),
						rs.getString("description"),
						rs.getString("release_year"));
				return film;
			} else {
				return null;
			} 
			
		} catch (SQLException e) {
	
	//e.printStackTrace();
			throw new RuntimeException(e);
		}
			
			
		
	}

}
