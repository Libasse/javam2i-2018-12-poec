package com.m2i.training.javase.oop.intermediate;

public class Note implements Comparable<Note> {
	private int value;

	public Note(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	@Override
	public int compareTo(Note o) {
		return  value - o.value;
	}
	
	

}
