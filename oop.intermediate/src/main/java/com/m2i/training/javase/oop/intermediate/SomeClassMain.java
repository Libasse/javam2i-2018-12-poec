package com.m2i.training.javase.oop.intermediate;

public class SomeClassMain {

	public static void main(String[] args) {
		
		SomeClass instance = new SomeClass();
		
		System.out.println(instance);
		
		Class clazz = instance.getClass();
		System.out.println(clazz.getName());
		
		System.out.println(instance.equals(instance));
		
		SomeClass instance1 = new SomeClass();
		SomeClass instance2 = new SomeClass();
		
		System.out.println(instance1.equals(instance2));
		
		
				

	}

}
