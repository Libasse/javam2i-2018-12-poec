package com.m2i.training.javase.oop.intermediate;

import java.util.ArrayList;



public class Playlist {
	
//	private songList[] choices;
	private ArrayList<Song> choices;
	
	
	public Playlist() {
		choices = new  ArrayList<Song>();
	}
	
	public void add (Song s1) {
		choices.add(s1);	
	}

	public ArrayList<Song> getchoices() {
		return choices;
	}
	
}
