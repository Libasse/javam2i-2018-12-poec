package com.m2i.training.javase.oop.intermediate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ComparableMain {
	
	public static void main(String[] args) {
		Set<Note> notes = new TreeSet<>();
		
		notes.add(new Note(10));
		notes.add(new Note(8));
		notes.add(new Note(15));
		notes.add(new Note(4));
		notes.add(new Note(18));
		
		for (Note note: notes) {
			System.out.println(note.getValue());
		}
		
		
		List<Note> noteList = new ArrayList<Note>(notes);
		
	}

}
