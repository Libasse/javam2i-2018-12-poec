package com.m2i.training.javase.oop.intermediate;

public class PersonMain {

	public static void main(String[] args) {
		
		
		Person p = new Person("Libasse", "Gadiaga");
		
		System.out.println(Person.DEFAULT_EMAIL);
		System.out.println(p.DEFAULT_EMAIL); // a eviter
		
		final int i = 0;
		// i = 4; // erreur de compilation
	}

}
