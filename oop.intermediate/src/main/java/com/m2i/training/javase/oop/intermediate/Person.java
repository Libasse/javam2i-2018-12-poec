package com.m2i.training.javase.oop.intermediate;

public class Person {
	
	public static final String DEFAULT_EMAIL = "person@exemple.com";
	
	private String firstname;
	private String lastname;
	
	
	
	public Person(String firstname, String lastname) {
		// super();
		this.firstname = firstname;
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
}
