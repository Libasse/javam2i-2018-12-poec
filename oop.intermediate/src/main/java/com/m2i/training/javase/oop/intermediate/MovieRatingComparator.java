package com.m2i.training.javase.oop.intermediate;

import java.util.Comparator;

public class MovieRatingComparator implements Comparator<Movie>{

	@Override
	public int compare(Movie m1, Movie m2) {
		return m1.getRating() - m2.getRating();
	}
	

}
