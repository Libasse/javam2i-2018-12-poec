package com.m2i.training.javase.oop.intermediate.sakila;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SakilaMain {

	public static void main(String[] args) {
		
		// TODO 
		/* ecrire un programme qui va lire tous les film
 			dans la BDD et les inserer dans la liste film
		 
		 */
		
		FilmRepository repo = new FilmRepository();
		List<Film> films = repo.findAll();
		
		
		for (Film film : films) {
			System.out.println(film);
		}

		Film film = repo.find(1); // trouve le film d'id 1
									// si n'existe pas retourne
		
		
		CustomerRepository customerRepo = new CustomerRepository();
		Customer customers = customerRepo.find(1);
		System.out.println(customers);
	}

}
