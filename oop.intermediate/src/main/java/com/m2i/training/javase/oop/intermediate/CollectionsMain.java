package com.m2i.training.javase.oop.intermediate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

public class CollectionsMain {

	public static void main(String[] args) {
		//ArrayList<String> strings = new ArrayList<String>();
		//LinkedList<String> strings = new LinkedList<String>();
		
		List<String> strings = new ArrayList<String>();
		
		strings.add("hello");
		strings.add("world");
		
		System.out.println(strings);
		
		// HashSet<String> setStrings = new HashSet<String>();
		TreeSet<String> setStrings = new TreeSet<String>();
		
		setStrings.add("hello 1");
		setStrings.add("world 1");
		
		System.out.println(setStrings);
		

	}

}
