package com.m2i.training.javase.oop.intermediate;

import java.util.HashMap;

public class MapMain {

	public static void main(String[] args) {
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("hello", "bonjour");
		map.put("bye", "au revoir");
		
		System.out.println(map.get("hello"));
		System.out.println(map.get("bye"));
		System.out.println(map.get("unknown"));
		
		System.out.println(map.size());
		System.out.println(map.isEmpty());
		
		
		for (String key : map.keySet()) {
			System.out.println(key);		
		}
		
		
		
		HashMap<Integer, Person> ranking = new HashMap<Integer, Person>();
		
		ranking.put(1, new Person("John", "Doe"));
		ranking.put(2, new Person("Emma", "Watson"));
		
		System.out.println(ranking.get(1).getLastname());
		System.out.println(ranking.get(2).getFirstname());
		
		
		
		
		
		
		
		
	}

}
