package com.m2i.training.javase.oop.intermediate;

public class DebuggerMain {

	public static void main(String[] args) {
		
		int i;
		i=3;
		System.out.println(i);
		
		int j = 4;
		System.out.println(j);
		
		i=j;
		System.out.println(i);
		
		Person person = new Person("Libasse", "Gadiaga");
		String name = person.getFirstname();
		String last = person.getLastname();
		
		for(int x=0; x < 5; x++) {
		System.out.println(x);
		
		}

	}

}
