package com.m2i.training.javase.oop.intermediate.sakila;

public class Customer {
	private int customer_id;
	private String first_name;
	private String last_name;
	private int address_id;
	private int country_id;
	private Country country;



	public Customer(int customer_id, String first_name, String last_name, int address_id, int country_id,
			String country) {
		super();
		this.customer_id = customer_id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.address_id = address_id;
		this.country_id = country_id;
		this.country = country;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public int getCountry_id() {
		return country_id;
	}

	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}




	@Override
	public String toString() {
		return "Customer [customer_id=" + customer_id + ", first_name=" + first_name + ", last_name=" + last_name
				+ ", country_id=" + country_id + ", country=" + country + "]";
	}

	
	
	
	
	
	
	
	
	

}
