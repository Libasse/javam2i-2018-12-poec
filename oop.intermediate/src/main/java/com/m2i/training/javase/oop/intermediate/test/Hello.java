package com.m2i.training.javase.oop.intermediate.test;

public class Hello {

	public String sayHello() {
		return "hello";
	}
	
	public String sayHello(String name) {
		if (name== null) {
			return "hello";
		}
		return "hello " + name;
	}
}
