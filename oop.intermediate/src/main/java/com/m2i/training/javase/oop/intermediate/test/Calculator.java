package com.m2i.training.javase.oop.intermediate.test;

public class Calculator {
	
	public int add(int a, int b) throws IllegalArgumentException {
		int result = a + b;
		if ( a>0 && b>0 && result <0 ) {
			throw new IllegalArgumentException("a + b > Integer.MAX_VALUE");
		}
		if ( a<0 && b<0 && result >=0 ) {
			throw new IllegalArgumentException("a + b < Integer.MAX_VALUE");
		}
			return result;
		
		
	}

}
