package com.m2i.training.javase.oop.intermediate;

public class ExceptionMain {

	public static void main(String[] args) {

		
		System.out.println("Avant");
		
//		int[] arr = new int[2];
//		arr[2]=42;
		
		
//		double rnd = Math.random();
		try {
			if (Math.random()> 0.5) {
			throw new RuntimeException("boom");
			}
			
	
		} catch (RuntimeException ex) {
			System.out.println(ex);
		}
		
		
		
		System.out.println("Après");

	}

}
