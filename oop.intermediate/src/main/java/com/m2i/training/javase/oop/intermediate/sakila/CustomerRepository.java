package com.m2i.training.javase.oop.intermediate.sakila;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerRepository {

	
	public Customer find(int customer_id) {
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?serverTimezone=UTC","root","rootroot")) {
					
					
					PreparedStatement  stmt = con.prepareStatement("SELECT * FROM customer WHERE customer_id= ?");
					//ResultSet rs=stmt.executeQuery("SELECT * FROM film WHERE film_id= ?");
					
					stmt.setInt(1, customer_id);
					ResultSet rs=stmt.executeQuery();
					if (rs.next()) {
						Customer customer = new Customer(rs.getInt("customer_id"),
								
								rs.getString("first_name"),
								rs.getString("last_name"),
								rs.getInt("address_id"),
								rs.getInt("country_id"),
								rs.getString("country"));
						return customer;
					} else {
						return null;
					} 
					
				} catch (SQLException e) {
			
			//e.printStackTrace();
					throw new RuntimeException(e);
				}
		
	}
	
}
