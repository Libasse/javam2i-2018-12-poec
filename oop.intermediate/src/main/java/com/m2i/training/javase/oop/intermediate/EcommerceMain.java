package com.m2i.training.javase.oop.intermediate;

public class EcommerceMain {

	public static void main(String[] args) {
//		on veut pouvoir acheter des livres
		
//		1) comment representer la notion de livres
		Book  book = new Book ("Java c'est bien");
		Book book2 = new Book ("Java 8 c'est bien");
		
//		2) comment ajouter des livres dans un panier d'achat
		ShoppingCart cart = new ShoppingCart();
		cart.add(book,1);
		cart.add(book2,5);
		
		
//		3) comment lister les livres disponibles dans un panier d'achat
		ShoppingCartItem[] items = cart.getItems();
		System.out.println(items.length);
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				System.out.println(items[i].getBook().getTitle() + " " + items[i].getQty());
			}
			
		}
		 
//		4) comment gérer la notion de quantité d'un livre dans un panier d'achat

	}

}
