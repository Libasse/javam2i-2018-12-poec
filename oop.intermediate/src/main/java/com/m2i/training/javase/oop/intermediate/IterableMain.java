package com.m2i.training.javase.oop.intermediate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class IterableMain {

	public static void main(String[] args) {
	
		List<String> strings = new ArrayList<String>();
		strings.add("hello");
		strings.add("world");
		
		Collection<String> asACollection = strings;
		Iterable<String> asAnIterable = strings;
		
		Iterator<String> iterator = asAnIterable.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
			// iterator.remove();
		}

		for (String s: asAnIterable) {
			System.out.println(s);
		}
		
		for (String s: strings) {
			System.out.println(s);
		}
	}

}
