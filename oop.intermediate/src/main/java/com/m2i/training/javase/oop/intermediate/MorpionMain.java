package com.m2i.training.javase.oop.intermediate;

public class MorpionMain {

	
	
	
	public static void main(String[] args) {
		
		
		// attribuer un symbole au joueur 1
		Player player1 = new Player();
		player1.setSymbol('X');
		
		// attribuer l'autre symbole au joueur 2
		Player player2 = new Player();
		player2.setSymbol('O');
		
		// currentPlayer = new Player
		Player currentPlayer = player1;
		
		// créer la grille
		Player[] [] grid = new Player[3] [3];
		grid [0] [0]=null;
		grid [0] [0]= player1;
		grid [0] [0]= player1;
		grid [0] [0]= player2;
		grid [0] [0]= player2;
		grid [0] [0]= player2;
		
		
		//Player[] grid = new Player[9];
		
		//tracer la grille
		
		System.out.println("-------------");
		System.out.println("| 0 | X | O |");
		System.out.println("-------------");
		System.out.println("| 0 | X | O |");
		System.out.println("-------------");
		System.out.println("| 0 | X | O |");
		System.out.println("-------------");
		}
		
		// compteurDeTour = 0
		
		// joueur 1 joue (place son symbole sur une case vide)
		// verifie si joueur 1 a gagné
		// si oui  : arreter le jeu
		// "passer la main"
		// compteurDeTour++
		
		// joueur 2 joue (place son symbole sur une case vide)
		// verifie si joueur 2 a gagné
		// si joueur 2 a gagné : arreter le jeu + msg bravo
		// passer la main
		// compteurDeTour++
				
		//System.out.println("on va débuter le jeu");
		
		

	} 

}
 