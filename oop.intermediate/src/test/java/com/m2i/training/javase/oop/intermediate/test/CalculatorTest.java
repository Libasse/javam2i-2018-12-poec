package com.m2i.training.javase.oop.intermediate.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

class CalculatorTest {

	@Test
	void testTwoMaxvalue() {	
		Calculator calcul= new Calculator();
		int a = 1;
		int b = Integer.MAX_VALUE;
		try {
			int result = calcul.add(a, b);
		} catch (IllegalArgumentException ex) {
			return;
		}
	
		fail("pas d'xeption");
	}

	
	
}
