package com.m2i.training.javase.oop.intermediate.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

class HelloTest {

	@Test
	void testSayHello() {
		//fail("Not yet implemented");
		
		// Arranger son test
		Hello hello= new Hello();
		
		// Agir 
		String result = hello.sayHello();
		
		// Assertions
		
	assertEquals("hello", result);		
	
	}
	
	@Test
	public void testsayHelloWithName() {
		
		//Arrange
		Hello hello= new Hello();
		
		// Act
		String result = hello.sayHello("Libasse");
		
		// Assertions
		
		assertEquals("hello Libasse", result);
	}
	
	
	@Test
	public void testSayHelloToNull() {
		//Arrange
		Hello hello= new Hello();
				
		// Act
		String result = hello.sayHello(null);
				
		// Assertions
				
		assertEquals("hello", result);
	}
	
	
	
	
	
	
	
	
	
	
	

}
