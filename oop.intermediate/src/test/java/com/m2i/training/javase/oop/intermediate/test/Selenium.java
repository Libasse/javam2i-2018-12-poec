package com.m2i.training.javase.oop.intermediate.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class Selenium {

	@Test
	void test() {
		fail("Not yet implemented");
	}
	
	@desabled
	@Test
	public void testGoogleSearch() throws InterruptedException {
	  // Optional, if not specified, WebDriver will search your path for chromedriver.
	  System.setProperty("webdriver.chrome.driver", "C:/Users/gg/Downloads/chromedriver.exe");

	  WebDriver driver = new ChromeDriver();
	  driver.get("http://www.google.com/xhtml");
	  Thread.sleep(5000);  // Let the user actually see something!
	  WebElement searchBox = driver.findElement(By.name("q"));
	  searchBox.sendKeys("ChromeDriver");
	  searchBox.submit();
	  Thread.sleep(5000);  // Let the user actually see something!
	  driver.quit();
	}
	
	@Test
	public void testGoogleSearch() throws InterruptedException {
	  // Optional, if not specified, WebDriver will search your path for chromedriver.
	  System.setProperty("webdriver.chrome.driver", "C:/Users/gg/Downloads/chromedriver.exe");

	  WebDriver driver = new ChromeDriver();
	  driver.get("http://example.com/");
	  assertEquals("Example Domain", driver.getTitle());
	  
	  //Thread.sleep(5000);  // Let the user actually see something!
	  WebElement h1 = driver.findElement(By.tagName("h1"));
	  assertEquals("Example Domain", h1.getText());
	  
	  WebElement a = driver.findElement(By.tagName("a"));
	  assertEquals("More information...", a.getText());
	  //searchBox.sendKeys("ChromeDriver");
	 // System.out.println(searchBox.getText());
	  //searchBox.submit();
	  //Thread.sleep(5000);  // Let the user actually see something!
	  driver.quit();
	}

}
