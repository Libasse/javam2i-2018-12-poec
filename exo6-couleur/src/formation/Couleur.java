package formation;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "couleur", eager = true)
@SessionScoped
public class Couleur {
	
	
	int maxcases = 20;
	List<Integer> lescases = null;
	int compteur = 0;
	
	
	public Couleur() {		
		lescases = new ArrayList<Integer>();
		for (int i =0; i<20; i++) {
			lescases.add(i);
		}
	}

	public int getMaxcases() {
		return maxcases;
	}

	public void setMaxcases(int maxcases) {
		this.maxcases = maxcases;
	}


	public List<Integer> getLescases() {
		return lescases;
	}

	public void deplace()
	{		
		compteur ++;
	}

	public String couleur (int unecase )
	{
		if (unecase == compteur ) return "yellow";		
		else return "gray"; 	
	}

}
	
	