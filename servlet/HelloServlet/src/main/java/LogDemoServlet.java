

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;

@WebServlet("/log")
public class LogDemoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(LogDemoServlet.class.getName());
	private static final org.slf4j.Logger LOGGER_SLF4J = LoggerFactory.getLogger(LogDemoServlet.class.getName());
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		LOGGER.fine("ceci est un log niveau fine");
		
		LOGGER.log(Level.INFO, "ceci est un log niveau info");
		
		LOGGER_SLF4J.trace("ceci est un log de niveau trace");
			
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		
	}
   
	

	

}
