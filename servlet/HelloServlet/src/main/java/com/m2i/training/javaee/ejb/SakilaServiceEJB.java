package com.m2i.training.javaee.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.m2i.training.com.javaee.jpa.Film;

@Stateless
public class SakilaServiceEJB {
	
	@PersistenceContext(unitName="pu1")
	private EntityManager entityManager;
	//Les methodes d'un EJB stateless sont appelées dans une transaction
	public  Film find(Long id) {
		return entityManager.find(Film.class, id);
		
	}
	
	public void addFilm(String title) {
		// TODO Auto-generated method stub
		Film f = new Film();
		f.setTitle(title);
		f.setLanguageId(1);
		entityManager.persist(f);
		
	}

}
