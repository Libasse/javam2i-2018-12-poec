package com.m2i.training.javaee.ejb;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.m2i.training.com.javaee.jpa.Film;

/**
 * Servlet implementation class JpaEjbServlet
 */
@WebServlet("/jpaejb")
public class JpaEjbServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   @Inject
	private SakilaServiceEJB sakilaService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Film f = sakilaService.find(new Long(1));
		response.getWriter().append(f.toString());
	}

	

}
