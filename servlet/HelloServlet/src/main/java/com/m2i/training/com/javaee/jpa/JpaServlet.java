package com.m2i.training.com.javaee.jpa;

import java.io.IOException;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

//import com.m2i.training.oop.Hibernate.Film;
import com.m2i.training.com.javaee.jpa.Film;

/**
 * Servlet implementation class JpaServlet
 */
@WebServlet("/jpa")
public class JpaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	@PersistenceUnit(unitName="pu1")
	private EntityManagerFactory entityManagerFactory;
       
  
	@Resource
	private UserTransaction utx ;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		//entityManager.getTransaction().begin();
		
			try {
				utx.begin();
			
			Film f = entityManager.find(Film.class, new Long(1));		
			//System.out.println(f);
			
			System.out.println(f);
			
			response.getWriter().append(f.toString());
			utx.commit();
			} catch (Exception e) {
				
			}
			
			
		
		
		
		
		
		//entityManager.getTransaction().commit();
		entityManager.close();
		
		
	}

	

}
