
public class Image {
	
	private int id;
	private String filename;
	private String title;
	private int category_id;
	
	public Image(int id, String filename, String title, int category_id) {
		super();
		this.id = id;
		this.filename = filename;
		this.title = title;
		this.category_id = category_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	@Override
	public String toString() {
		return "Image [id=" + id + ", filename=" + filename + ", title=" + title + ", category_id=" + category_id + "]";
	}
	
	

}
