

import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Servlet implementation class ImageServlet
 */
@WebServlet("/images")
public class ImagesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	

		ImageRepository ImageRepo = new ImageRepository();
		
		List<Image> images = ImageRepo.findAll(); 
		
		request.setAttribute("images", images);
		request.getRequestDispatcher("/WEB-INF/Images.jsp")
		   .forward(request, response);
		
		// JDBC
		
		//response.getWriter().append("images");
		
		/*response.setHeader("Content-type", "text/html");
		PrintWriter out = response.getWriter();
		out.append("<!doctype html>")
			.append("<html>")
			.append("<body>");
		out.append("<table>");	
		 // JDBC
				for (Image i: images) {
					response.getWriter()
					.append("<tr>")
					.append("<td>"+ i.getId() + "</td>")
					.append("<td>"+ i.getFilename() + "</td>")
					.append("<td>"+ i.getTitle() + "</td>")
					.append("<td>"+ i.getCategory_id() + "</td>")
					.append("</tr>");
					out.append("</table>")
					.append("</body>")
					.append("</html>");	
				}*/
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//request.setAttribute("image", images);
		
		

	
	}

	

}
