

import java.io.IOException;
import java.io.PrintWriter;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FilmsServlet
 */
@WebServlet("/films")
public class FilmsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// declarer le connecteur mysql dans le pom.xml
		
		FilmRepository filmRepo=new FilmRepository();
		List<Film> films = filmRepo.findAll();	
		
		response.setHeader("Content-type", "text/html");		
		PrintWriter out = response.getWriter();		
		out.append("<!doctype html>")
			.append("<html>")
			.append("<body>");		
		out.append("<table>");			
		 // JDBC
				for (Film f: films) {
					response.getWriter()
					.append("<tr>")
					.append("<td>"+ f.getFilmId() + "</td>")
					.append("<td>"+ f.getTitle() + "</td>")
					.append("</tr>");									
				}			
		out.append("</table>")
		.append("</body>")
		.append("</html>");		
	}
}
