

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddFilmServlet
 */
@WebServlet("/add-film")
public class AddFilmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.getRequestDispatcher("WEB-INF/add-film.jsp")
				.forward(request, response);
	}
	
private Map<String, String> validateForm(HttpServletRequest request) {
		
		Map<String, String> errors 
			= new HashMap<String, String>();

		// title
		String title = request.getParameter("title");
		
		if(title == null || title.isEmpty()) {
			errors.put("title", "le titre ne doit pas être vide");
		} else if (title.length() < 2) {
			errors.put("title", "la longueur du titre doit être > 2");
		}
		
		// description
		String description = request.getParameter("description");
		
		if(description == null || description.isEmpty()) {
			errors.put("description", "la description ne doit pas être vide");
		}
		
		return errors;
	}
	
	/*private boolean validateForm(HttpServletRequest request) {
		String title=request.getParameter("title");
		if (title==null) {
			return false;
		}
		if (title.length() < 2) {
			return false;
		}
		
		String description=request.getParameter("description");
		if (description==null  || description.isEmpty()) {
			return false;
		}
		if (description.length() < 2) {
			return false;
		}
		
		
		return true;
	}*/
	
	
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		/*boolean isFormValid= validateForm(request);
		if (! isFormValid) {*/
			
		Map<String, String> formErrors = validateForm(request);
		if(! formErrors.isEmpty()) {
			request.setAttribute("formErrors", formErrors);
			request.getRequestDispatcher("WEB-INF/add-film.jsp")
				.forward(request, response);
			return;	
		}
		
		
		// ecrire dans la base de donnees le film
		Film film= new Film();
		// appeler les stteurs pour remplir le film à partir de l'objet request
		
		FilmRepository filmRepository= new FilmRepository();
		try {
			filmRepository.add(film);
		} catch (RuntimeException ex) {
			return;
			
		}
		
		response.sendRedirect("films");
		// si l'ecriture marche rediriger vers une autre page
		// sinon  retourner 'oups
	}

	

}
