

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ImageServlet
 */
@WebServlet("/image/*")
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		
		
		String pathInfo= request.getPathInfo();
		if (pathInfo==null) {
			response.setStatus(404);
			return; // ne pas continuer après le if !!!
		}
		pathInfo=pathInfo.substring(1);
		
		int id;
		id= Integer.parseInt(pathInfo);
		ImageRepository imageRepo=new ImageRepository();
		Image image = imageRepo.find(id);
		if (image==null) {
			response.setStatus(404);
			return;
		}
		
		
		
		request.setAttribute("image", image);
		
		request.getRequestDispatcher("/WEB-INF/Image.jsp")
			   .forward(request, response);
		
		/*response.setHeader("Content-type", "text/html");
		PrintWriter out = response.getWriter();
		out.append("<!doctype html>")
		.append("<html>")
		.append("<body>");
		out.append("<table>");
		out.append("<tr>")
		.append("<td>"+ image.getId() + "</td>")
		.append("<td>"+ image.getFilename() + "</td>")
		.append("<td>"+ image.getTitle() + "</td>")
		.append("<td>"+ image.getCategory_id() + "</td>")
		.append("</tr>");
		out.append("</table>")
		.append("</body>")
		.append("</html>"); */
		
		
		
		
		
	
		
		
		//out.append("<h1>"+ image.getId()+"</h1>");
		//out.append("<p>"+ image.getFilename()+"</p>");
		
		
		
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		
		
		//out.append("<p>"+request.getRequestURI()+"</p>");
		//out.append("<p>"+request.getPathInfo()+"</p>");
		//out.append("<p>"+request.getPathTranslated()+"</p>");
		
		
		
		
		
		
	}

	

}
