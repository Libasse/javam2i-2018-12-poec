

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/api/hello")
public class HelloRestWebService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");  // UTF-8 pour JSON
		response.setHeader("Content-Type" ,"application/json");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.getWriter()
		.append("{\"served-at\":\"" + request.getContextPath() +"\"}");
		
		
		
		
		//{"served-at":"api/hello"}
		//.append("Served at: ").append(request.getContextPath());
	}

	

}
