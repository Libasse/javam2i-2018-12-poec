

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AboutServlet
 */
@WebServlet("/about")
public class AboutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*response.getWriter().append("Served at: ").append(request.getContextPath());*/
		response.setHeader("Content-Type", "text/html");
		response
		.getWriter()
		.append("<!doctype html>")
		.append("<html>")
		.append("<body>")
		.append("<a href=\"http://localhost:8080/servlet-0.0.1-SNAPSHOT/about\">about</a>")
		/*.append("<p>about</p>")*/
		.append("</body>")
		.append("</html>");
		
	}

	

}
