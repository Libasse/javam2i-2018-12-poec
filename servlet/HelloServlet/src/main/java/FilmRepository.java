

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



/* Disclaimer: ce n'est pas un vrai repository comme vous 
	 * auriez dans spring Framework, ou ce 
	 
	 */
public class FilmRepository {
	
	public List<Film> findAll(int from, int count){
		
		List<Film> films = new ArrayList<Film>();
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?serverTimezone=UTC","root","rootroot")) {
			String query = "SELECT * FROM film LIMIT ?, ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, from);
			stmt.setInt(2, count);
			
		
			ResultSet rs=stmt.executeQuery();
			while (rs.next()) {
				//System.out.println(rs.getString("title"));
				Film film = new Film(rs.getInt("film_id"),
									rs.getString("title"),
									rs.getString("description"),
									rs.getString("release_year"));
						films.add(film);
			}
			
			
				
			} catch (SQLException e) {
				
				//e.printStackTrace();
				throw new RuntimeException(e);
			}
		return films;
	}
			
	public List<Film> findAll(){
		
List<Film> films = new ArrayList<Film>();
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?serverTimezone=UTC","root","rootroot")) {
			String query = "SELECT * FROM film";
			PreparedStatement stmt = con.prepareStatement(query);
			
			
		
			ResultSet rs=stmt.executeQuery();
			while (rs.next()) {
				//System.out.println(rs.getString("title"));
				Film film = new Film(rs.getInt("film_id"),
									rs.getString("title"),
									rs.getString("description"),
									rs.getString("release_year"));
						films.add(film);
			}
			
			
				
			} catch (SQLException e) {
				
				//e.printStackTrace();
				throw new RuntimeException(e);
			}
		return films;
	}

	public Film find(int id) {
try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila?serverTimezone=UTC","root","rootroot")) {			
			
			PreparedStatement  stmt = con.prepareStatement("SELECT * FROM film WHERE film_id= ?");
			//ResultSet rs=stmt.executeQuery("SELECT * FROM film WHERE film_id= ?");			
			stmt.setInt(1, id);
			ResultSet rs=stmt.executeQuery();
			if (rs.next()) {
				Film film = new Film(rs.getInt("film_id"),
						rs.getString("title"),
						rs.getString("description"),
						rs.getString("release_year"));
				return film;
			} else {
				return null;
			} 
			
		} catch (SQLException e) {
	
	//e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
			
	public void add(Film film) {
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sakila?serverTimezone=UTC",
				"root", "rootroot")) {
			
			PreparedStatement stmt = 
					con.prepareStatement(
							"INSERT INTO sakila.film (title, language_id) VALUE (?, ?)");
			stmt.setString(1, film.getTitle());
			stmt.setInt(2, 1); // language id à 1
			
			int count =  stmt.executeUpdate();
			
		} catch (SQLException e) {
			// en pratique on va plutôt créer une RepositoryException
			throw new RuntimeException(e);
		}
		
	}
	
}
			
		

