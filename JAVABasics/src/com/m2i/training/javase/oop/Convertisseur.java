package com.m2i.training.javase.oop;

public class Convertisseur {

	public double convert(double value, String fromUnit, String toUnit) {
		
		
		if (fromUnit == toUnit) {
			return value;
		} 
		
		if (fromUnit == "cm" && toUnit == "in") {
			double valeurEnInch = value / 2.54;
			return valeurEnInch;
		}
		
		
		if (fromUnit == "in" && toUnit == "cm") {
			double valeurEnCm = value * 2.54;
			return valeurEnCm;
		}
		
		
		
		
		return -1; // en attente de savoir g�rer les erreurs
		
		
		
		
	}

}
