package com.m2i.training.javase.oop;

public class DogMain {

	public static void main(String[] args) {
		
	System.out.println("Dans main de dogMain");
	
	// Dog d1 = new Dog();
	Dog d1;  //declaration d'une variable d1 de type dog
	d1 = new Dog(); // creation d'un nouvel objet Dog et affectation de cet objet � la variable d1
	Dog d2 = new Dog();
	Dog d3 = new Dog();
	
	System.out.println(d1);
	System.out.println(d2);
	System.out.println(d3);
	
	d1.name = "Medor";
	d2.name = "Fido";
	d3.name = "Rantanplan";
	
	System.out.println(d1.name);
	System.out.println(d2.name);
	System.out.println(d3.name);
	
	d1.color = "brown";
	d2.color = "green";
	d3.color = "blue";
	
	d1.ishungry = true;
	d2.ishungry = true;
	d3.ishungry = false;
	
	System.out.println(d1.ishungry);
	System.out.println(d2.ishungry);
	System.out.println(d3.ishungry);
	
	
	
	
	}

}
