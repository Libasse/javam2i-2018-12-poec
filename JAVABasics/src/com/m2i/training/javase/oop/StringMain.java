package com.m2i.training.javase.oop;

public class StringMain {

	public static void main(String[] args) {
		
		String s1 = "Hello Java";
		String s2 = new String ("Hello Java");
		
		System.out.println(s1);
		System.out.println(s2);
		
		int length = s1.length();
		System.out.println(length);
		 char c0= s1.charAt(0);
		 System.out.println(c0);
		 System.out.println(s1.charAt(1));
		 
		 // exercice : Afficher chaque caractere  de s1 sur une ligne
		 // 
		 System.out.println("-----------");
		 System.out.println("-----------");
		 System.out.println("-----------");
		 
		 int i;
		 
		 for (i=0; i < length; i++) {
			 System.out.println(s1.charAt(i));
		 }
		 
		 // exo 2: afficher les memes caracteres dans l'ordre inverse
		 
		 System.out.println("-----------");
		 System.out.println("-----------");
		 System.out.println("-----------");
		 for (i= length - 1; i >= 0; i--) {
			 System.out.println(s1.charAt(i));
		 }
		 
		 System.out.println("-----------");
		 System.out.println("-----------");
		 System.out.println("-----------");
		 
		 // exo 3. afficher que les voyelles
		 
		 
		 for (i= s1.length() - 1; i >= 0; i--) {
			 char v= s1.charAt(i); 
			 if (v == 'a' || v == 'e' || v == 'i' || v == 'o' || v == 'u' || v == 'y') { 
			 System.out.println(v);
			 }
			 
		 }
		 
		 System.out.println("-----------");
		 System.out.println("-----------");
		 System.out.println("-----------");
		 
		 // exo4: String mot =" .... "
		 // afficher "est un palindrome" si mot est un palindrome;
		// afficher "n'est pas un palindrome" si mot n'est pas un palindrome;
		 
		 String mot = "ade";
		 double median = mot.length()/2;
		 boolean isPalindrome = true;
		 
		 for (i = 0; i < median; i++) {
			 
			if (mot.charAt(i) != mot.charAt(mot.length()-1- i)) {
				isPalindrome = false;
				break;
			} 
		 }
		 
		System.out.println(isPalindrome);
		 
		System.out.println("-----------");
		 System.out.println("-----------");
		 System.out.println("-----------");
		 
		 
		 String s3 = "pas vide";
		 String s4 = "";
		 System.out.println("s3 est vide ? " + s3.isEmpty());
		 System.out.println("s4 est vide ? " + s4.isEmpty());
		 
		 System.out.println(s3.substring(3));
		 System.out.println(s3.substring(2, 5));
		 
		 System.out.println("chaine 1 " + "chaine 2");
		 System.out.println(s1.concat(s2));
		 

	}
}
