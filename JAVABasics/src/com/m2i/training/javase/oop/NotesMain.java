package com.m2i.training.javase.oop;

public class NotesMain {

	public static void main(String[] args) {
		
		int[] notes = {12,16,7,18,4,9,11};
		
//		1) calculer la somme des notes
		int sum = 0;
		for (int i = 0; i < notes.length; i++) {
			 sum += notes[i];
		}
		System.out.println(sum);
//		2) la moyenne
		System.out.println(sum/notes.length);
//		3) la note max
		int value = notes[0];
		for (int i = 0; i < notes.length; i++) {
			if (notes[i] > value) {
				value = notes[i];
			}
		}
		System.out.println(value);
		
//		4) la note min
		
		int value1 = notes[0];
		for (int i = 0; i < notes.length; i++) {
			if (notes[i] < value1) {
				value1 = notes[i];
			}
		}
		System.out.println(value1);
		
	}

}
