package com.m2i.training.javase.oop;

public class BicycletteMain {

	public static void main(String[] args) {
	
	Bicyclette A1 = new Bicyclette();
	Bicyclette A2 = new Bicyclette();
	Bicyclette A3 = new Bicyclette();
	
	
	System.out.println(A1);
	System.out.println(A2);
	System.out.println(A3);
	
	A1.currentSpeed = 2;
	A2.currentSpeed = 3;
	A3.currentSpeed = 5;
	
	A1.numAvant = 2;
	A2.numAvant = 3;
	A3.numAvant = 5;

	}

}
