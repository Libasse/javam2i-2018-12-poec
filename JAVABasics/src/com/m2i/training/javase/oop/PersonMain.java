package com.m2i.training.javase.oop;

public class PersonMain {

	public static void main(String[] args) {
		Person p = new Person();
		System.out.println(p);
		
		//p.firstname = "Libasse";
		//p.lastname = "Gadiaga";
		
		//System.out.println(p.firstname);
		//System.out.println(p.lastname);
		
		//String result = p.getFirstname();
		//System.out.println(p.getFirstname());
		
		//String result2 = p.getLastname();
		//System.out.println(p.getLastname());
		
		String result3 = p.getFullname();
		System.out.println(p.getFullname());
		
		String result4 = p.getFullnameWithPrefix("Mr. ");
		System.out.println(result4);
		
		
		p.setFirstname("Bob");
		System.out.println(p.getFirstname());
		
		p.setLastname("Marley");
		System.out.println(p.getLastname());
		

	}

}
