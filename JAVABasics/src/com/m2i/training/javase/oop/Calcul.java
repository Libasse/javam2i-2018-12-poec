package com.m2i.training.javase.oop;

public class Calcul {
	
	//////////////////////////////////////////////////////////////////////////
	
	private boolean isVowel (char c) {
		return c == 'a' || c == 'e' || c == 'i'
			|| c == 'o' || c == 'u' || c == 'y';
	}
	
	///////////////////////////////////////////////////////////////////////
	
	public int vowelsCount(String data) {
		int length = data.length();
		int i;
		
		int cpt1 = 0;	
		for (i=0; i < length; i++) {
			 char v= data.charAt(i);			  
			 //if (v == 'a' || v == 'e' || v == 'i' || v == 'o' || v == 'u' || v == 'y') {
			 if (isVowel(v)) {
				 
				 cpt1= cpt1 + 1; 
			 }	
		}					
		return cpt1;		
	}

	///////////////////////////////////////////////////////////////////////
	
	public double Percent(String data) {
		int vowelsCount = vowelsCount(data);
		int length = data.length();
		int i;
		double cpt2 = 0;
		
		for (i=0; i < length; i++) {
			 char v= data.charAt(i);			  
			 if (v != ' ') {
				 cpt2= cpt2 + 1; 
			 }	
		}	
		
		return vowelsCount/cpt2;				
	}

	///////////////////////////////////////////////////////////////////////////////
	public double getTotalVowelsCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	////////////////////////////////////////////////////////////////////////////
	
	public double AverageVowelsCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	/////////////////////////////////////////////////////////////////////////////
	
	
}
