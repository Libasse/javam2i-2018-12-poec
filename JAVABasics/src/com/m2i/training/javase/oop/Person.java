package com.m2i.training.javase.oop;

public class Person {
	// 1) variable d'instance
	private String firstname;
	private String lastname;
	
	// 2) getters/ setters
	
	public String getFirstname() {
		return firstname;
	}
	
	
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	
	public String getLastname() {
		return lastname;
	}
	
	
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	
	// 3) autres lethodes
	
	
	//public String getFirstname() {
		//return firstname;
	//}

	//public String getLastname() {
		//return lastname;
	//}
	
	// Ecrire methode getFullname qui retourne la concatenation nom et prenom
	
	public String getFullname() {
		return firstname + " " + lastname;
	}
	
	
	// ecrire une methode getFullnamePrefix qui prend en
	// parametre un prefixe de type string ( ex: Mr. Ms.)
	// retourne le full name pr�fix� du pr�fixe
	
	


	public String getFullnameWithPrefix( String prefix) {
		return  prefix + " " + firstname + " " + lastname;
	}


	
	// ecrire une methode setFirstname qui prend en parametre un nouveau firstname
	// et met a jour le firstname de l'instance
	
	//public void  setFirstname( String newFirstname) {
		//firstname = newFirstname;
	/*}
	
	
	public void  setLastname( String newLastname) {
		lastname = newLastname;
	}*/
	
}



