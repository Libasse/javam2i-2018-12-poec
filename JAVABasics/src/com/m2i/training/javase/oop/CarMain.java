package com.m2i.training.javase.oop;

public class CarMain {

	public static void main(String[] args) {
		car car1 = new car();
		System.out.println(car1.speed); // 0
		car1.accelerate();
		System.out.println(car1.speed);
		car1.accelerate();
		System.out.println(car1.speed);
		//car1.brake();
		System.out.println(car1.speed);
		//car1.brake();
		System.out.println(car1.speed);
		// int uneAutrevariable;
		// System.out.println(uneAutrevariable);
		
		car1.speedLimiterOn = true;
		car1.speed = 10;
		
		//car1.speedLimiterOn = false;
		// Max = 20
		
		car1.accelerate();
		
		System.out.println(car1.speed);
		
		car1.accelerate();
		System.out.println(car1.speed);
		
		car1.accelerate();
		System.out.println(car1.speed);
		
		car1.speed =10;
		System.out.println(car1.speed);
		car1.brake(5); 
		System.out.println(car1.speed);
		car1.brake(7); 
		System.out.println(car1.speed);

		
		
	}

}
