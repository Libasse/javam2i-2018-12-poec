package com.m2i.training.javase.oop;

public class NumberMain {

	public static void main(String[] args) {
//		byte, short, long; char; double; float; boolean; int
		
//		Types Wrapper:
//		Byte, Short, Long; Char; Double; Float; Boolean; Int
		
		int i1 = 1;
		Integer i2 = new Integer(1);
		
		int i3 = new Integer(1); // unboxing
		// (new Integer(1).intvalue();
		Integer i4 = 1;
		// new Integer(1)
		
		// methode statique
		System.out.println(Integer.MIN_VALUE);
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.toHexString(123123));
		System.out.println(Integer.compare(4,10));
		
		// methode d'instance
		Integer i5 = new Integer(4);
		Integer i6 = new Integer(10);
		System.out.println(i5.compareTo(i6));
	}

}
