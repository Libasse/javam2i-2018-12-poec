package com.m2i.training.javase.basics;

public class PrimitiveTypeMain {

	public static void main(String[] args) {
		// 8 types primitifs
		
		// boolean : true/false
		boolean b1= true;
		boolean b2= false;
		
		System.out.println("-----------");
		System.out.println("Booleans:");
		System.out.println(b1);
		System.out.println(b2);
		
		// byte, short, int, long
		// repésentent des nombres entiers
		
		System.out.println("-----------");
		System.out.println("Entiers:");
		int i1 = 12345; //32 bits
		System.out.println(i1);
		
		short s1 = 123; //16 bits
		System.out.println(s1);
		
		byte by1 =12; //8 bits
		System.out.println(by1);
		
		long l1 = 123456789; //64 bits
		System.out.println(l1);
		long l2= 123456789L;
		
		int iDec = 26; // 6*10^0 + 2*10^1
		int iHex = 0x1a; // a(10)*16^0+1*16^1
		int iBin =  0b11010; // 0*2^0+1*2^0+1*2^1+0*2^2++1*2^3+1*2^4
		int ioct= 032; // base octal
		// float, double
		
		System.out.println("-----------");
		System.out.println("décimaux:");
		float f1 = 12.57f;
		System.out.println(f1);
		
		double d1= 14.46;
		System.out.println(d1);
		
		double d2 = 123.4D;
		double d3 = 1.234e2; // notation scientifique	
				
		System.out.println("-----------");
		System.out.println("caractères");
		// char
		char c1 = 'a'; // caractere unicode
		char c2 = '\u0108'; //unicode 'escape'
		
		System.out.println(c1); 
		
		// Depuis java 7, pour une meilleure lisibilité
		// on peut utiliser des _ entre les chiffres
		int i2= 100_000_000;

	}

}
