package JAVABasics.src.com.m2i.training.javase.basics;

public class OperatorMain {

	public static void main(String[] args) {
		int i1 = 32;
		int i3=3;
		i3 +=10;
		System.out.println(i3);
		
		int i4= i3 += 5;
		System.out.println(i3);
		System.out.println(i4);
		
		// assigment, arithmetics, unary
		//
		System.out.println(1/2); // division enti�re
		System.out.println(1.0/2); // flottant
		System.out.println(1/2.0);
		System.out.println(1f/2);
		System.out.println(1/2d);
		//docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html
		
		
		int i5 = 2+3 ;
		double i6 = 2.0 +3;
		String concat = "bonjour" + 3 ; // concatenation
		System.out.println(concat);
		
		String concat2 = "bonjour" + 2.0 ; // concatenation
		System.out.println(concat2);
		
		// modulo %
		
		
		
		int m1 = 4 % 2;
		System.out.println("m1 =" + m1);
		
		int m2 = 5 % 2;
		System.out.println("m2 =" + m2);
		
		// unaires
		int i7 = -5;
		System.out.println("i7=" + i7);
		
		int i8 = i7++;
		System.out.println("i7=" + i7);
		System.out.println("i8=" + i8);
		
		int i9 = ++i7;
		System.out.println("i7=" + i7);
		System.out.println("i9=" + i9);
		
		int i10 = i7--;
		System.out.println("i7=" + i7);
		System.out.println("i10=" + i10);
		
		int i11 = --i7;
		System.out.println("i7=" + i7);
		System.out.println("i11=" + i11);
		
		// https://docs.oracle.com/javase/tutorial/java/nutsandbolts/op2.html
		// Equality, Relational, and conditional operators
		
		boolean res1 = 5 == 5;
		System.out.println("5 == 5" + res1);

		boolean res2 = 5 == 4;
		System.out.println("5 == 4" + res2);
		
		int i12 = 42;
		
		boolean res3 = i12 == i6;
		System.out.println("i12 == i6" + res3);
		
		boolean res4 = i12 != i6;
		System.out.println("i12 != i6" + res4);
		
		boolean res5 = i12 > i6;
		boolean res6 = i12 >= i6;
		boolean res7 = i12 < i6;
		boolean res8 = i12 <= i6;
		
		// conditional operators
		
		System.out.println("----------------");
		
		System.out.println("true && true =" + (true && true));
		System.out.println("true && false =" + (true && false));
		System.out.println("false && true =" + (false && true));
		System.out.println("false && false =" + (false && false));
		
		System.out.println("----------------");
		System.out.println("true || true =" + (true || true));
		System.out.println("true || false =" + (true || false));
		System.out.println("false || true =" + (false || true));
		System.out.println("false || false =" + (false || false));
		
		System.out.println("----------------");
		System.out.println("true & true =" + (true & true));
		System.out.println("true | true =" + (true | true));
		
		System.out.println("----------------");
		System.out.println("true ^(XOR) true =" + (true ^ true));
		System.out.println("true ^(XOR) false =" + (true ^ false));
		System.out.println("false ^(XOR) true =" + (false ^ true));
		System.out.println("false ^(XOR) false =" + (false ^ false));
		
		// ^ est equivalent � != pour si operandes boolean
		
		boolean b1 = (true || false) && (false || true);
		boolean b2 = (i4 == i3) && (i5 == i8);
		boolean b3 = (i4 == i3) || (i5 == i8);
		
		
		
		
		
		
	}
}
	
