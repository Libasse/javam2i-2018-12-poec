package com.m2i.training.javase.basics;

public class VariableMain {

	public static void main(String[] args) {
		
		// variable = nom utilis� pour simplifier l'acc�s � de la donn�e stock�e en m�moire
		String message = "Hello Java";
		
		String unAutreMesage; // d�claration
		unAutreMesage= "coucou"; // affectation/ assignation
		
		String encoreUnMessage;
		encoreUnMessage= unAutreMesage;
		
		unAutreMesage = "j'ai change";
		
		System.out.println(message);
		
		System.out.println(unAutreMesage);
		System.out.println(encoreUnMessage);
		
		

	}

}
