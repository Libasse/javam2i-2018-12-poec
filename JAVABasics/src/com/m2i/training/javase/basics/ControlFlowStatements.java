package com.m2i.training.javase.basics;

public class ControlFlowStatements {

	public static void main(String[] args) {
		
		 boolean ilFaitBeau = true;
		 
		 //if (ilFaitBeau  == true) ( pas la peine de mettre true)
		 if (ilFaitBeau) {
			 System.out.println("il fait beau");
		 } else {
			 System.out.println("il pleut");
		 }
		 if (ilFaitBeau) {
			 System.out.println("ah oui,il fait beau");
		 }
		 
			 System.out.println("on va travailler");
			 
			 
		int temp = 25;
		if (temp < 15 ) {
			 System.out.println("brrrr...");
		 } else if (temp < 20) {
			 System.out.println("il fait doux");
		 } else if (temp < 25) {
			 System.out.println("on peut sortir");
		 } else {
			 System.out.println("on pose un RTT"); 
		 }
		 
		int compteur = 0; // compteur est local � main
		for (;; ) {
			System.out.println("boucle infinie" + compteur);
			compteur++; // compteur +=1; // compteur= compteur +1;
			if (compteur==3) {
			break;
			}
		}
		
		for (int compteur2 = 0; compteur2 < 3; compteur2++) {
			System.out.println("boucle infinie" + compteur2);
			 // compteur +=1; // compteur= compteur +1;	
		}
		
		
		System.out.println("fin de la boucle");
		
		// SWITCH
		 int rating = 1;
		 String message;
		 
		 switch (rating) {
		 case 0:
			 message = "nul";
			 break;
		 case 1:
			 message = "bof";
			 break;
		 case 2:
			 message = "moyen";
			 break;
		 case 3:
			 message = "bien";
			 break;
		 case 4:
			 message = "tres bien"; 
			 break;
		 default:
			message = "rating incorrect";
			break;
		 
		 }
		
		 System.out.println(message);
		 
		 // WHILE
		 int compteur3 = 0;
		 
		 while (true) {
			 System.out.println("boucle infinie");
			 compteur3++;
			 if (compteur3==3) {
				 break;
			 }
			 
		 }
		 
		 int compteur4 = 0;
		 while (compteur4 < 3) {
			 System.out.println("boucle while" + compteur4);
			 compteur4++;
		 }
		 
		 
		 int compteur5 = 0;
		 do {
			 System.out.println("boucl do while3" + compteur5);
			 compteur5++;
		 }  while (compteur5 < 3);
		 
		 
		 
	}

}
