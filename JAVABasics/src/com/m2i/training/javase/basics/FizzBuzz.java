package JAVABasics.src.com.m2i.training.javase.basics;

public class FizzBuzz {

	public static void main(String[] args) {
		
		
		// Afficher tous les nombres de 0 � 100 (inclus)
		// pour chaque nombre
		// si le nombre est multiple de 3 afficher FIZZ <nombre>
		// ex: FIZZ 3
		// si le nombre est multiple de 5 afficher BUZZ <nombre>
		// ex: BUZZ 5
		// si le nombre est multiple de 3 rt 5 afficher FIZZBUZZ <nombre>
				// ex: FIZZBUZZ 15
		// sinon afficher le nombre
		// ex: 2
		
		for (int i = 0; i <= 100; i++) {
			int n = i % 3;
			int m = i % 5;
			if (n == 0 && m != 0) {
				System.out.println("FIZZ" + i);
			} else if (m == 0 && n != 0) {
				System.out.println("BUZZ" + i);
			} else if ( n == 0 && m == 0) {
				System.out.println("FIZZBUZZ" + i);
			} else {
				System.out.println(i);
			}
		
		}

	}

}
